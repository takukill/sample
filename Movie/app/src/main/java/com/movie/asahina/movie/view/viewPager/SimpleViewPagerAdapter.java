package com.movie.asahina.movie.view.viewPager;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.function.Consumer;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.view.Utils.VariableLayoutPair;

import java.util.Collection;

/**
 * Created by takuasahina on 2017/11/09.
 */

public class SimpleViewPagerAdapter<T> extends PagerAdapter implements View.OnClickListener {

	public SimpleViewPagerAdapter(ViewPager viewPager, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		this(viewPager, null, itemCollection, variableId, layoutId, onItemClick);
	}

	public SimpleViewPagerAdapter(ViewPager viewPager, final ViewPagerIndicator indicator, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		super();

		context = viewPager.getContext();

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		variableLayoutPair = new VariableLayoutPair(variableId, layoutId);

		if (itemCollection instanceof ObservableList) {
			itemList = (ObservableList<T>) itemCollection;
		} else {
			itemList = new ObservableArrayList<>();
			itemList.addAll(itemCollection);
		}

		this.onItemClick = onItemClick;

		viewPager.setOffscreenPageLimit(3);

		itemList.addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<T>>() {
			@Override
			public void onChanged(ObservableList<T> sender) {
				notifyDataSetChanged();
				if (indicator != null) {
					indicator.setCount(itemList.size());
				}
			}

			@Override
			public void onItemRangeChanged(ObservableList<T> sender, int positionStart, int itemCount) {
				notifyDataSetChanged();
				if (indicator != null) {
					indicator.setCount(itemList.size());
				}
			}

			@Override
			public void onItemRangeInserted(ObservableList<T> sender, int positionStart, int itemCount) {
				notifyDataSetChanged();
				if (indicator != null) {
					indicator.setCount(itemList.size());
				}
			}

			@Override
			public void onItemRangeMoved(ObservableList<T> sender, int fromPosition, int toPosition, int itemCount) {
				notifyDataSetChanged();
				if (indicator != null) {
					indicator.setCount(itemList.size());
				}
			}

			@Override
			public void onItemRangeRemoved(ObservableList<T> sender, int positionStart, int itemCount) {
				notifyDataSetChanged();
				if (indicator != null) {
					indicator.setCount(itemList.size());
				}
			}
		});
	}


	private final Context context;

	private final LayoutInflater inflater;

	private final VariableLayoutPair variableLayoutPair;

	private final ObservableList<T> itemList;

	private final Consumer<Pair<Integer, T>> onItemClick;


	//アイテム追加後に呼ばれる
	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		T itemViewModel = itemList.get(position);

		ViewDataBinding binding = DataBindingUtil.inflate(inflater, variableLayoutPair.layoutId, null, false);
		binding.setVariable(variableLayoutPair.variableId, itemViewModel);

		View view = binding.getRoot();

		if (onItemClick != null) {
			view.setTag(R.id.key_simple_view_pager_adapter_position, position);
			view.setTag(R.id.key_simple_view_pager_adapter_item, itemViewModel);
			view.setOnClickListener(this);
		}

		ViewPager pager = (ViewPager) container;
		pager.addView(view);

		return view;
	}


	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}


	@Override
	public int getCount() {
		if (itemList == null) {
			return 0;
		}

		return itemList.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view.equals(object);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((View) object);
	}


	@Override
	public void onClick(View v) {
		if (onItemClick == null) {
			return;
		}

		onItemClick.accept(new Pair<>((Integer) v.getTag(R.id.key_simple_view_pager_adapter_position), (T) v.getTag(R.id.key_simple_view_pager_adapter_item)));
	}
}