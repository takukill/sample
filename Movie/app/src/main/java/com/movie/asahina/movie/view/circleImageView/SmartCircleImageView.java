package com.movie.asahina.movie.view.circleImageView;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;

import com.loopj.android.image.ContactImage;
import com.loopj.android.image.SmartImage;
import com.loopj.android.image.SmartImageTask;
import com.loopj.android.image.WebImage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SmartCircleImageView extends de.hdodenhof.circleimageview.CircleImageView {

	public SmartCircleImageView(Context context) {
		super(context);
	}

	public SmartCircleImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public SmartCircleImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	private static final int LOADING_THREADS = 4;

	private static ExecutorService threadPool = Executors.newFixedThreadPool(4);

	private SmartImageTask currentTask;


	public void setImageUrl(String var1) {
		this.setImage(new WebImage(var1));
	}

	public void setImageUrl(String var1, Integer var2) {
		this.setImage(new WebImage(var1), var2);
	}

	public void setImageUrl(String var1, Integer var2, Integer var3) {
		this.setImage(new WebImage(var1), var2, var3);
	}

	public void setImageContact(long var1) {
		this.setImage(new ContactImage(var1));
	}

	public void setImageContact(long var1, Integer var3) {
		this.setImage(new ContactImage(var1), var3);
	}

	public void setImageContact(long var1, Integer var3, Integer var4) {
		this.setImage(new ContactImage(var1), var3, var3);
	}

	public void setImage(SmartImage var1) {
		this.setImage(var1, (Integer)null, (Integer)null);
	}

	public void setImage(SmartImage var1, Integer var2) {
		this.setImage(var1, var2, var2);
	}

	public void setImage(SmartImage var1, final Integer var2, Integer var3) {
		if(var3 != null) {
			this.setImageResource(var3.intValue());
		}

		if(this.currentTask != null) {
			this.currentTask.cancel();
			this.currentTask = null;
		}

		this.currentTask = new SmartImageTask(this.getContext(), var1);
		this.currentTask.setOnCompleteHandler(new SmartImageTask.OnCompleteHandler() {
			public void onComplete(Bitmap var1) {
				if(var1 != null) {
					SmartCircleImageView.this.setImageBitmap(var1);
				} else if(var2 != null) {
					SmartCircleImageView.this.setImageResource(var2.intValue());
				}

			}
		});
		threadPool.execute(this.currentTask);
	}

	public static void cancelAllTasks() {
		threadPool.shutdownNow();
		threadPool = Executors.newFixedThreadPool(4);
	}
}