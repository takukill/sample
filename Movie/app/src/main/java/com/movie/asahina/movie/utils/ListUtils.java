package com.movie.asahina.movie.utils;

import java.util.List;

/**
 * java.util.List のユーティリティ
 */
public class ListUtils {

	/**
	 * 古いリストを新しいリストで置き換えます。
	 * 具体的な操作としては、可能な限り set メソッドで上書きして、要素数が増えた場合は足りない分だけ add して、減った場合は不要な分だけ remove します。
	 * 最終的な結果は、古いリストを clear して、新しいリストを addAll したものと同じになりますが、ObservableList を BindingRecyclerView で使っている場合は、
	 * 途中の操作によって RecyclerView の挙動が変わり、clear, addAll だとスクロール位置が先頭までリセットされてしまうため、clear しない replace 方法としてこのメソッドを用意しています。
	 */
	public static <T> void replace(List<T> oldList, List<T> newList) {
		int oldListSize = oldList.size();
		int newListSize = newList.size();

		for (int i = 0; i < Math.max(oldListSize, newListSize); i++) {
			if (i < Math.min(oldListSize, newListSize)) {
				oldList.set(i, newList.get(i));
			} else if (oldListSize < newListSize) {
				oldList.add(newList.get(i));
			} else {
				oldList.remove(oldList.size() - 1);
			}
		}
	}
}
