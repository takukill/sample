package com.movie.asahina.movie.model.module;

import javax.inject.Singleton;

import dagger.Module;

/**
 * Dagger2で注入するオブジェクトを指定する。
 */

@Module
public class ModelModule {

//	@Provides
//	@Singleton
//	public EvaluationsModel provideEvaluationModel(EvaluationsApi evaluationsApi) {
//		return new EvaluationsModel(evaluationsApi);
//	}
}