package com.movie.asahina.movie.utils;

import android.support.v4.BuildConfig;
import android.util.Log;

/**
 * Created by takuasahina on 2018/02/21.
 */

public class LogUtils {

	public static void v(Class className, String log) {
		Log.d("aaa", "v: ");
		if (BuildConfig.DEBUG) {
			Log.v("qiilo", className.getSimpleName() + log);
		}
	}
}
