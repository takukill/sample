package com.movie.asahina.movie.preferences;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger2 で注入する preferences パッケージのオブジェクトを指定する。
 */
@Module
public class PreferencesModule {

	public PreferencesModule(Context context) {
		this.context = context;
	}

	private final Context context;

	@Singleton
	@Provides
	public Preferences providePreferences() {
		return new Preferences(context);
	}
}