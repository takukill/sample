package com.movie.asahina.movie.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.movie.asahina.movie.R;

/**
 * RecyclerView 用 inset divider。
 */
public class InsetDividerItemDecoration extends RecyclerView.ItemDecoration {

    public InsetDividerItemDecoration(Context context) {
        TypedArray typedArray = context.obtainStyledAttributes(new int[]{android.R.attr.listDivider});
        divider = typedArray.getDrawable(0);
        typedArray.recycle();
    }

    private final Drawable divider;

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (parent.getAdapter() instanceof SectionedRecyclerViewAdapter) {
            // SectionedRecyclerViewAdapter の場合、subheader かどうか判別できるので、material design guidelines に合わせて、subheader の下には divider を引かないようにする。
            SectionedRecyclerViewAdapter sectionedRecyclerViewAdapter = (SectionedRecyclerViewAdapter) parent.getAdapter();

            int left = parent.getPaddingLeft() + (int) parent.getResources().getDimension(R.dimen.inset_divider_padding_left);
            int right = parent.getWidth() - parent.getPaddingRight();

            for (int i = 0; i < parent.getChildCount(); i++) {
                View child = parent.getChildAt(i);
                if (sectionedRecyclerViewAdapter.isSubheader(parent.getChildAdapterPosition(child))) {
                    // subheader なので divider は引かない。
                    continue;
                }

                int top = child.getBottom() + ((RecyclerView.LayoutParams) child.getLayoutParams()).bottomMargin;
                int bottom = top + divider.getIntrinsicHeight();
                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        } else {
            int left = parent.getPaddingLeft() + (int) parent.getResources().getDimension(R.dimen.inset_divider_padding_left);
            int right = parent.getWidth() - parent.getPaddingRight();

            for (int i = 0; i < parent.getChildCount(); i++) {
                View child = parent.getChildAt(i);
                int top = child.getBottom() + ((RecyclerView.LayoutParams) child.getLayoutParams()).bottomMargin;
                int bottom = top + divider.getIntrinsicHeight();
                divider.setBounds(left, top, right, bottom);
                divider.draw(c);
            }
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(0, 0, 0, divider.getIntrinsicHeight());
    }

    public static class InsetDividerItemDecorationBindingAdapter {

        private static void addInsetDividerItemDecoration(RecyclerView recyclerView) {
            if (recyclerView.getTag(R.id.key_list_divider_item_decoration) != null) {
                return;
            }

            InsetDividerItemDecoration itemDecoration = new InsetDividerItemDecoration(recyclerView.getContext());
            recyclerView.addItemDecoration(itemDecoration);
            recyclerView.setTag(R.id.key_list_divider_item_decoration, itemDecoration);
        }

        private static void removeInsetDividerItemDecoration(RecyclerView recyclerView) {
            InsetDividerItemDecoration itemDecoration = (InsetDividerItemDecoration) recyclerView.getTag(R.id.key_list_divider_item_decoration);
            if (itemDecoration == null) {
                return;
            }

            recyclerView.removeItemDecoration(itemDecoration);
            recyclerView.setTag(R.id.key_list_divider_item_decoration, null);
        }

        @BindingAdapter("insetDividerEnabled")
        public static void setInsetDividerEnabled(RecyclerView recyclerView, boolean enabled) {
            if (enabled) {
                addInsetDividerItemDecoration(recyclerView);
            } else {
                removeInsetDividerItemDecoration(recyclerView);
            }
        }
    }
}