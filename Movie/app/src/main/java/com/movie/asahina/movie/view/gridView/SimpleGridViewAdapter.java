package com.movie.asahina.movie.view.gridView;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.annimon.stream.function.Consumer;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.view.Utils.VariableLayoutPair;

import java.util.Collection;

/**
 * Created by takuasahina on 2017/11/10.
 */

public class SimpleGridViewAdapter<T> extends BaseAdapter implements View.OnClickListener {

	public SimpleGridViewAdapter(GridView gridView, Collection<T> itemCollection, int variableId, @LayoutRes int layoutId, @Nullable Consumer<Pair<Integer, T>> onItemClick) {
		super();

		context = gridView.getContext();

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		variableLayoutPair = new VariableLayoutPair(variableId, layoutId);

		if (itemCollection instanceof ObservableList) {
			itemList = (ObservableList<T>) itemCollection;
		} else {
			itemList = new ObservableArrayList<>();
			itemList.addAll(itemCollection);
		}

		this.onItemClick = onItemClick;

		itemList.addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<T>>() {
			@Override
			public void onChanged(ObservableList<T> sender) {
				notifyDataSetChanged();
			}

			@Override
			public void onItemRangeChanged(ObservableList<T> sender, int positionStart, int itemCount) {
				notifyDataSetChanged();
			}

			@Override
			public void onItemRangeInserted(ObservableList<T> sender, int positionStart, int itemCount) {
				notifyDataSetChanged();
			}

			@Override
			public void onItemRangeMoved(ObservableList<T> sender, int fromPosition, int toPosition, int itemCount) {
				notifyDataSetChanged();
			}

			@Override
			public void onItemRangeRemoved(ObservableList<T> sender, int positionStart, int itemCount) {
				notifyDataSetChanged();
			}
		});
	}


	private final Context context;

	private final LayoutInflater inflater;

	private final VariableLayoutPair variableLayoutPair;

	private final ObservableList<T> itemList;

	private final Consumer<Pair<Integer, T>> onItemClick;


	public int getCount() {
		return itemList.size();
	}

	public Object getItem(int position) {
		return itemList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		T itemViewModel = itemList.get(position);

		ViewDataBinding binding = DataBindingUtil.inflate(inflater, variableLayoutPair.layoutId, null, false);
		binding.setVariable(variableLayoutPair.variableId, itemViewModel);

		View view = binding.getRoot();

		if (onItemClick != null) {
			view.setTag(R.id.key_simple_grid_view_adapter_position, position);
			view.setTag(R.id.key_simple_grid_view_adapter_item, itemViewModel);
			view.setOnClickListener(this);
		}

		return view;
	}


	@Override
	public void onClick(View v) {
		if (onItemClick == null) {
			return;
		}

		onItemClick.accept(new Pair<>((Integer) v.getTag(R.id.key_simple_grid_view_adapter_position), (T) v.getTag(R.id.key_simple_grid_view_adapter_item)));
	}
}