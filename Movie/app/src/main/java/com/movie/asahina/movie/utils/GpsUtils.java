package com.movie.asahina.movie.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.Locale;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by yutowatanabe on 2018/02/08.
 */

public class GpsUtils {

	public static int LOCATION_REQUEST_CODE = 1000;

	private static LocationManager locationManager;

	private static LocationListener locationListener;

	public static boolean isLocationEnable(Activity activity) {
		locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);

		final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		if (!gpsEnabled) {
			// GPSを設定するように促す
			Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			activity.startActivity(settingsIntent);
			return false;
		}

		if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, LOCATION_REQUEST_CODE);
			return false;
		}

		return true;
	}

	public static void startLocationListener(Activity activity, LocationListener listener) {
		locationListener = listener;
		if (isLocationEnable(activity)) {
			locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_COARSE);
			criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
			criteria.setBearingRequired(false);
			criteria.setSpeedRequired(false);
			criteria.setAltitudeRequired(false);
			String provider = locationManager.getBestProvider(criteria, true);
			locationManager.requestLocationUpdates(provider, 0, 0, listener);
		}
	}

	public static void removeLocationListener(Activity activity) {
		if (locationManager != null && locationListener != null) {
			locationManager.removeUpdates(locationListener);
			locationManager = null;
			locationListener = null;
		}
	}

	public static String getAddress(Context context, double latitude, double longitude) {
		try {
			Geocoder geocoder = new Geocoder(context, Locale.getDefault());
			Address addresse = geocoder.getFromLocation(latitude, longitude, 1).get(0);
			return "〒" + addresse.getPostalCode() + "\n" + addresse.getAdminArea() + addresse.getLocality() + addresse.getSubLocality() + addresse.getSubThoroughfare();
		} catch (Exception e) {
		}
		return "";
	}
}

