package com.movie.asahina.movie.view.collapsingToolbar;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.util.AttributeSet;
import android.view.ViewParent;

/**
 * Created by takuasahina on 2018/01/10.
 */

public class SmartCollapsingToolbarLayout extends CollapsingToolbarLayout {

	private AppBarLayout.OnOffsetChangedListener mOnOffsetChangedListener;

	public SmartCollapsingToolbarLayout(final Context context) {
		super(context);
	}

	public SmartCollapsingToolbarLayout(final Context context, final AttributeSet attrs) {
		super(context, attrs);
	}

	public SmartCollapsingToolbarLayout(final Context context, final AttributeSet attrs, final int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		ViewParent parent = this.getParent();
		if (parent instanceof AppBarLayout) {
			if (mOnOffsetChangedListener != null)
				((AppBarLayout) parent).addOnOffsetChangedListener(mOnOffsetChangedListener);
		}
	}

	public void setOnOffsetChangeListener(AppBarLayout.OnOffsetChangedListener listener) {
		mOnOffsetChangedListener = listener;
	}
}