package com.movie.asahina.movie.databinding;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.Pair;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.movie.asahina.movie.BaseApplication;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.view.recyclerview.DataBindingRecyclerViewUtils;

public class DataBindingExtensions {

	//region View Extensions
	@BindingAdapter("isInvisible")
	public static void setInvisible(View view, boolean invisible) {
		view.setVisibility(invisible ? View.INVISIBLE : View.VISIBLE);
	}

	@BindingAdapter("isGone")
	public static void setGone(View view, boolean gone) {
		view.setVisibility(gone ? View.GONE : View.VISIBLE);
	}

	@BindingAdapter("backgroundResource")
	public static void setBackgroundResource(View view, int resourceId) {
		view.setBackgroundResource(resourceId);
	}
	//endregion

	@BindingAdapter("javaScriptEnabled")
	public static void setJavaScriptEnabled(WebView webView, boolean enable) {
		webView.getSettings().setJavaScriptEnabled(enable);
	}
	//endregion

	@BindingAdapter("refreshing")
	public static void setRefreshing(SwipeRefreshLayout swipeRefreshLayout, boolean refreshing) {
		swipeRefreshLayout.setRefreshing(refreshing);
	}

	@BindingAdapter("onRefreshListener")
	public static void setOnRefreshListener(SwipeRefreshLayout swipeRefreshLayout, SwipeRefreshLayout.OnRefreshListener onRefreshListener) {
		swipeRefreshLayout.setOnRefreshListener(onRefreshListener);
	}
	//endregion


	/**
	 * marginTopを指定する。
	 *
	 * @param view
	 * @param margin
	 */
	@BindingAdapter("android:layout_marginTop")
	public static void setLayoutMarginTop(View view, float margin) {
		ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
		if (lp != null) {
			lp.setMargins(lp.leftMargin, (int) margin, lp.rightMargin, lp.bottomMargin);
			view.setLayoutParams(lp);
		}
	}
	//endregion

	@BindingAdapter("expandedTitle")
	public static void setExpandedTitle(CollapsingToolbarLayout toolbarLayout, String title) {
		toolbarLayout.setTitle(title);
		toolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
		toolbarLayout.setContentScrimColor(BaseApplication.getContext().getResources().getColor(R.color.white));
		toolbarLayout.setCollapsedTitleTextColor(BaseApplication.getContext().getResources().getColor(R.color.greyish_brown_444444));
	}

	@BindingAdapter("title")
	public static void setExpandedTitle(Toolbar toolbar, String title) {
		toolbar.setTitle(title);
	}


	@BindingAdapter({"toolbarIconColor"})
	public static void setToolbarIconColor(Toolbar toolbar, Integer textColor) {
		if (textColor != null) {
			Drawable navigationIcon = toolbar.getNavigationIcon();
			if (navigationIcon != null) {
				navigationIcon = DrawableCompat.wrap(navigationIcon);
				DrawableCompat.setTint(navigationIcon.mutate(), ContextCompat.getColor(toolbar.getContext(), textColor));
				toolbar.setNavigationIcon(navigationIcon);
			}
			Drawable overflowIcon = toolbar.getOverflowIcon();
			if (overflowIcon != null) {
				overflowIcon = DrawableCompat.wrap(overflowIcon);
				DrawableCompat.setTint(navigationIcon.mutate(), ContextCompat.getColor(toolbar.getContext(), textColor));
				toolbar.setOverflowIcon(overflowIcon);
			}
		}
	}

	@BindingAdapter({"android:enable"})
	public static void setEnabled(View view, boolean enable) {
		view.setEnabled(enable);
	}

//	@BindingAdapter({"balloonList"})
//	public static void test(RecyclerView view, ObservableList<NotificationItemViewModel> list) {
//		DataBindingRecyclerViewUtils.bind(view, list, BR.viewModel, R.layout.item_notification, new Consumer<Pair<Integer, NotificationItemViewModel>>() {
//			@Override
//			public void accept(Pair<Integer, NotificationItemViewModel> pair) {
//				NotificationItemViewModel item = pair.second;
//				item.selectItem();
//			}
//		});
//	}
}
