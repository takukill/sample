package com.movie.asahina.movie.view.Utils;

/**
 * Created by takuasahina on 2017/11/22.
 */

public interface ScrollEvent {

	void onScrollTop(boolean top);

	void onScrollBottom(boolean bottom);

}
