package com.movie.asahina.movie.view.viewPager.peeping;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

/**
 * Created by takuasahina on 2017/12/08.
 */
public class PeepingViewPager extends ViewPager {

	public PeepingViewPager(Context context) {
		super(context);
		init(context);
	}

	public PeepingViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		this.setClipToPadding(false);
		this.setPadding(80, 0, 80, 0);
	}
}
