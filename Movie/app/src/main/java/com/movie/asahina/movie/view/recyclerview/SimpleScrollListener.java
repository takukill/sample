package com.movie.asahina.movie.view.recyclerview;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.movie.asahina.movie.view.Utils.ScrollEvent;


/**
 * Created by takuasahina on 2017/11/22.
 */

public class SimpleScrollListener extends RecyclerView.OnScrollListener {

	public SimpleScrollListener(ScrollEvent scrollEvent) {
		this.scrollEvent = scrollEvent;
	}


	@Override
	public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
		super.onScrolled(recyclerView, dx, dy);

		int totalCount = recyclerView.getAdapter().getItemCount();
		int childCount = recyclerView.getChildCount();
		RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();

		if (layoutManager instanceof GridLayoutManager) {
			GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
			int firstPosition = gridLayoutManager.findFirstVisibleItemPosition();

			scrollEvent.onScrollTop(firstPosition == 0);
			scrollEvent.onScrollBottom(totalCount == firstPosition + childCount);

		} else if (layoutManager instanceof LinearLayoutManager) {
			LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
			int firstPosition = linearLayoutManager.findFirstVisibleItemPosition();

			scrollEvent.onScrollTop(firstPosition == 0);
			scrollEvent.onScrollBottom(totalCount == firstPosition + childCount);
		}
	}


	private final ScrollEvent scrollEvent;
}
