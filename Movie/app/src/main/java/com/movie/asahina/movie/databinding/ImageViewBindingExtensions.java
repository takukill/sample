package com.movie.asahina.movie.databinding;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.text.TextUtils;
import android.widget.ImageView;

import com.loopj.android.image.SmartImageView;
import com.movie.asahina.movie.view.circleImageView.SmartCircleImageView;


public class ImageViewBindingExtensions {

	//region ImageView Extensions
	@BindingAdapter("imageResource")
	public static void setImageResource(ImageView imageView, @DrawableRes int resourceId) {
		imageView.setImageResource(resourceId);
	}

	@BindingAdapter("imageBitmap")
	public static void setImageResource(ImageView imageView, Bitmap bitmap) {
		if (bitmap == null) {
			return;
		}

		imageView.setImageBitmap(bitmap);
	}

	@BindingAdapter("loadImage")
	public static void setLoadImage(SmartImageView imageView, String urlOrUri) {
		if (TextUtils.isEmpty(urlOrUri)) {
			return;
		}

		Uri uri = Uri.parse(urlOrUri);
		if (uri.getScheme().equals("http") || uri.getScheme().equals("https")) {
			imageView.setImageUrl(urlOrUri);
		} else {
			imageView.setImageURI(uri);
		}
	}

	@BindingAdapter({"imageBitmap", "loadImage"})
	public static void setImageBitmap(SmartImageView imageView, Bitmap bitmap, String urlOrUri) {
		if (bitmap != null) {
			setImageResource(imageView, bitmap);
		} else {
			setLoadImage(imageView, urlOrUri);
		}
	}

	@BindingAdapter("loadCircleImage")
	public static void loadCircleImage(SmartCircleImageView imageView, String urlOrUri) {
		if (TextUtils.isEmpty(urlOrUri)) {
			return;
		}

		Uri uri = Uri.parse(urlOrUri);
		if (uri.getScheme().equals("http") || uri.getScheme().equals("https")) {
			imageView.setImageUrl(urlOrUri);
		} else {
			imageView.setImageURI(uri);
		}
	}

	@BindingAdapter("circleImageBitmap")
	public static void loadCircleImageBitmap(SmartCircleImageView imageView, Bitmap bitmap) {
		if (bitmap == null) {
			return;
		}

		imageView.setImageBitmap(bitmap);
	}

	@BindingAdapter({"loadCircleImage", "circleImageBitmap"})
	public static void circleImageBitmap(SmartCircleImageView imageView, Bitmap bitmap, String urlOrUri) {
		if (bitmap != null) {
			loadCircleImageBitmap(imageView, bitmap);
		} else {
			loadCircleImage(imageView, urlOrUri);
		}
	}

	@BindingAdapter("imageTint")
	public static void setImageTint(ImageView imageView, @ColorInt int colorInt) {
		Drawable drawable = imageView.getDrawable();
		drawable.setColorFilter(colorInt, PorterDuff.Mode.SRC_IN);
		imageView.setImageDrawable(drawable);
	}

	@BindingAdapter("imageTintString")
	public static void setImageTint(ImageView imageView, String colorString) {
		int color = Color.parseColor(colorString);

		Drawable drawable = imageView.getDrawable();
		drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
		imageView.setImageDrawable(drawable);
	}

	@BindingAdapter("colorFilter")
	public static void setColorFilterString(ImageView imageView, String colorString) {
		int color = Color.parseColor(colorString);
		imageView.setColorFilter(color, PorterDuff.Mode.SRC_IN);
	}
}
