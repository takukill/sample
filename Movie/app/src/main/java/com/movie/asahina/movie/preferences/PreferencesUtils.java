package com.movie.asahina.movie.preferences;

import android.text.TextUtils;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * アプリの SharedPreferences のユーティリティ
 */
public class PreferencesUtils {


	//region 種
	private static final byte[] SECRET_KEY_SRC = "ABCDEFGHIJKLMNOP".getBytes();

	private static final byte[] SECRET_IV_SRC = "QRSTUVWXYZabcdef".getBytes();
	//endregion

	/**
	 * 保存前の一手間
	 */
	static String daVinci(String target) {
		byte[] daVinci;

		if (TextUtils.isEmpty(target)) {
			return target;
		}

		Cipher cipher;
		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			return null;
		}

		SecretKeySpec key = new SecretKeySpec(SECRET_KEY_SRC, "AES");
		IvParameterSpec iv = new IvParameterSpec(SECRET_IV_SRC);

		try {
			cipher.init(Cipher.ENCRYPT_MODE, key, iv);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			return null;
		}

		try {
			daVinci = cipher.doFinal(target.getBytes());
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			return null;
		}

		return Base64.encodeToString(daVinci, Base64.NO_WRAP);
	}

	/**
	 * 取得前の一手間
	 */
	static String langdon(String target) {
		byte[] langdon;

		if (TextUtils.isEmpty(target)) {
			return target;
		}

		SecretKeySpec key = new SecretKeySpec(SECRET_KEY_SRC, "AES");
		IvParameterSpec iv = new IvParameterSpec(SECRET_IV_SRC);

		Cipher decrypt;
		try {
			decrypt = Cipher.getInstance("AES/CBC/PKCS7Padding");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			return null;
		}

		try {
			decrypt.init(Cipher.DECRYPT_MODE, key, iv);
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			return null;
		}

		byte[] daVinci = Base64.decode(target, Base64.NO_WRAP);

		try {
			langdon = decrypt.doFinal(daVinci);
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			return null;
		}

		try {
			return new String(langdon, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}
	//endregion
}