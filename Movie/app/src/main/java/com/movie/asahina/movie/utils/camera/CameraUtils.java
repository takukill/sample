package com.movie.asahina.movie.utils.camera;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;

/**
 * Created by takuasahina on 2017/11/06.
 */

public class CameraUtils {

	public CameraUtils(Activity activity) {
		this.activity = activity;
	}


	private final Activity activity;

	private TextureViewUtils textureViewUtils;

	private SurfaceViewUtils surfaceViewUtils;


	public void openCamera(SurfaceView surfaceView, TextureView textureView) {
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//			textureViewUtils = new TextureViewUtils(activity, cameraListener);
//			textureViewUtils.openCamera(textureView);
//
//			surfaceView.setVisibility(View.GONE);
//
//		} else {
		surfaceViewUtils = new SurfaceViewUtils(activity);
		surfaceViewUtils.openCamera(surfaceView);

		textureView.setVisibility(View.GONE);
//		}
	}

	public void setCameraListener(CameraListener cameraListener) {
		surfaceViewUtils.setCameraListener(cameraListener);
	}

	public void setQRReader(QRReader listener) {
		surfaceViewUtils.setQRReader(listener);
	}

	public void takePicture(int width, int height, int x, int y, int widthScale, int heightScale) {
		surfaceViewUtils.takePicture(width, height, x, y, widthScale, heightScale);
	}

	public void closeCamera() {
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//			textureViewUtils.closeCamera();
//		} else {
		surfaceViewUtils.closeCamera();
//		}
	}

	public void setTorchMode(boolean on) {
		surfaceViewUtils.setTorchMode(on);
	}


	public interface CameraListener {

		void onCameraTakePicture(Bitmap bitmap);

	}

	public interface QRReader {

		void onReadQRCode(String text);

	}
}
