package com.movie.asahina.movie.utils;

import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiFunction;
import com.annimon.stream.function.Function;

import java.util.List;

/**
 * android.databinding.ObservableList ユーティリティ
 */
public class ObservableListUtils {

	/**
	 * コールバックをセットしたリストの変更に追従するようなコールバック
	 *
	 * @param <T> 先導するリストの型
	 * @param <U> 追従するリストの型
	 */
	public static class FollowingCallback<T, U> extends ObservableList.OnListChangedCallback<ObservableList<T>> {

		public FollowingCallback(List<U> follower, @NonNull Function<T, U> mapper) {
			this.follower = follower;
			this.onChangedMapper = null;
			this.onInsertedMapper = mapper;
		}

		/**
		 * OnItemRangeChanged のときに追従するリストの要素の現在の値が必要な場合はこちらのコンストラクタを用いる
		 */
		public FollowingCallback(List<U> follower, @NonNull BiFunction<T, U, U> onChangedMapper, @NonNull Function<T, U> onInsertedMapper) {
			this.follower = follower;
			this.onChangedMapper = onChangedMapper;
			this.onInsertedMapper = onInsertedMapper;
		}

		/**
		 * 追従するリスト
		 */
		private final List<U> follower;

		/**
		 * onItemRangeChanged のときに、先導するリストの要素を追従するリストの要素の変換する関数
		 * 第二引数は追従するリストの要素の現在の値
		 * この関数が null のときは onItemRangeChanged のときにも onInsertedMapper を使う
		 */
		@Nullable
		private final BiFunction<T, U, U> onChangedMapper;


		/**
		 * onItemRangeInserted のときに、先導するリストの要素を追従するリストの要素の変換する関数
		 * onChangedMapper が null のときは onItemRangeChanged のときにもこの関数を使う
		 */
		@NonNull
		private final Function<T, U> onInsertedMapper;

		@Override
		public void onChanged(ObservableList<T> sender) {
			// do nothing
		}

		@Override
		public void onItemRangeChanged(ObservableList<T> sender, int positionStart, int itemCount) {
			if (onChangedMapper == null) {
				for (int i = positionStart; i < positionStart + itemCount; i++) {
					follower.set(i, onInsertedMapper.apply(sender.get(i)));
				}
			} else if (follower.size() != sender.size()) {
				// onChangedMapper はセットされているけど、先導するリストと追従するリストの状態が異なる場合
				// この場合、追従するリストをクリアして、先導するリストを onInsertedMapper でマップしたものをすべて追加する
				follower.clear();
				follower.addAll(Stream.of(sender)
						.map(onInsertedMapper)
						.collect(Collectors.<U>toList()));
			} else {
				for (int i = positionStart; i < positionStart + itemCount; i++) {
					follower.set(i, onChangedMapper.apply(sender.get(i), follower.get(i)));
				}
			}
		}

		@Override
		public void onItemRangeInserted(ObservableList<T> sender, int positionStart, int itemCount) {
			for (int i = positionStart; i < positionStart + itemCount; i++) {
				follower.add(i, onInsertedMapper.apply(sender.get(i)));
			}
		}

		@Override
		public void onItemRangeMoved(ObservableList<T> sender, int fromPosition, int toPosition, int itemCount) {
			throw new UnsupportedOperationException(ON_ITEM_RANGE_MOVED_EXCEPTION_MESSAGE);
		}

		@Override
		public void onItemRangeRemoved(ObservableList<T> sender, int positionStart, int itemCount) {
			for (int i = 0; i < itemCount; i++) {
				follower.remove(positionStart);
			}
		}
	}

	/**
	 * リストに何かしらの変更（要素の変更、追加、削除）があったときに呼ばれるコールバック
	 *
	 * @param <T> リスト要素の型
	 */
	public abstract static class OnListChangedCallback<T> extends ObservableList.OnListChangedCallback<ObservableList<T>> {

		public abstract void onListChanged(int startPosition, int itemCount);

		@Override
		public final void onChanged(ObservableList<T> sender) {
			// do nothing
		}

		@Override
		public final void onItemRangeChanged(ObservableList<T> sender, int startPosition, int itemCount) {
			onListChanged(startPosition, itemCount);
		}

		@Override
		public final void onItemRangeInserted(ObservableList<T> sender, int startPosition, int itemCount) {
			onListChanged(startPosition, itemCount);
		}

		@Override
		public final void onItemRangeMoved(ObservableList<T> sender, int positionFrom, int positionTo, int itemCount) {
			throw new UnsupportedOperationException(ON_ITEM_RANGE_MOVED_EXCEPTION_MESSAGE);
		}

		@Override
		public final void onItemRangeRemoved(ObservableList<T> sender, int startPosition, int itemCount) {
			onListChanged(startPosition, itemCount);
		}
	}

	/**
	 * リスト要素に変更があったときに呼ばれるコールバック
	 *
	 * @param <T> リスト要素の型
	 */
	public abstract static class OnItemRangeChangedCallback<T> extends ObservableList.OnListChangedCallback<ObservableList<T>> {

		public abstract void onChanged(int startPosition, int itemCount);

		@Override
		public final void onChanged(ObservableList<T> sender) {
			// do nothing
		}

		@Override
		public final void onItemRangeChanged(ObservableList<T> sender, int startPosition, int itemCount) {
			onChanged(startPosition, itemCount);
		}

		@Override
		public final void onItemRangeInserted(ObservableList<T> sender, int startPosition, int itemCount) {
			// do nothing
		}

		@Override
		public final void onItemRangeMoved(ObservableList<T> sender, int positionFrom, int positionTo, int itemCount) {
			throw new UnsupportedOperationException(ON_ITEM_RANGE_MOVED_EXCEPTION_MESSAGE);
		}

		@Override
		public final void onItemRangeRemoved(ObservableList<T> sender, int startPosition, int itemCount) {
			// do nothing
		}
	}

	/**
	 * リストに要素が挿入されたときに呼ばれるコールバック
	 *
	 * @param <T> リスト要素の型
	 */
	public abstract static class OnItemRangeInsertedCallback<T> extends ObservableList.OnListChangedCallback<ObservableList<T>> {

		public abstract void onInserted(int startPosition, int itemCount);

		@Override
		public final void onChanged(ObservableList<T> sender) {
			// do nothing
		}

		@Override
		public final void onItemRangeChanged(ObservableList<T> sender, int startPosition, int itemCount) {
			// do nothing
		}

		@Override
		public final void onItemRangeInserted(ObservableList<T> sender, int startPosition, int itemCount) {
			onInserted(startPosition, itemCount);
		}

		@Override
		public final void onItemRangeMoved(ObservableList<T> sender, int positionFrom, int positionTo, int itemCount) {
			throw new UnsupportedOperationException(ON_ITEM_RANGE_MOVED_EXCEPTION_MESSAGE);
		}

		@Override
		public final void onItemRangeRemoved(ObservableList<T> sender, int startPosition, int itemCount) {
			// do nothing
		}
	}

	/**
	 * リストから要素が削除されたときに呼ばれるコールバック
	 *
	 * @param <T> リスト要素の型
	 */
	public abstract static class OnItemRangeRemovedCallback<T> extends ObservableList.OnListChangedCallback<ObservableList<T>> {

		public abstract void onRemoved(int startPosition, int itemCount);

		@Override
		public final void onChanged(ObservableList<T> sender) {
			// do nothing
		}

		@Override
		public final void onItemRangeChanged(ObservableList<T> sender, int startPosition, int itemCount) {
			// do nothing
		}

		@Override
		public final void onItemRangeInserted(ObservableList<T> sender, int startPosition, int itemCount) {
			// do nothing
		}

		@Override
		public final void onItemRangeMoved(ObservableList<T> sender, int positionFrom, int positionTo, int itemCount) {
			throw new UnsupportedOperationException(ON_ITEM_RANGE_MOVED_EXCEPTION_MESSAGE);
		}

		@Override
		public final void onItemRangeRemoved(ObservableList<T> sender, int startPosition, int itemCount) {
			onRemoved(startPosition, itemCount);
		}
	}

	/**
	 * リストのサイズが変更されたときに呼ばれるコールバック
	 *
	 * @param <T> リスト要素の型
	 */
	public abstract static class OnListSizeChangedCallback<T> extends ObservableList.OnListChangedCallback<ObservableList<T>> {

		public abstract void onListSizeChanged(int itemCount);

		@Override
		public final void onChanged(ObservableList<T> sender) {
			// do nothing
		}

		@Override
		public final void onItemRangeChanged(ObservableList<T> sender, int startPosition, int itemCount) {
			// do nothing
		}

		@Override
		public final void onItemRangeInserted(ObservableList<T> sender, int startPosition, int itemCount) {
			onListSizeChanged(itemCount);
		}

		@Override
		public final void onItemRangeMoved(ObservableList<T> sender, int positionFrom, int positionTo, int itemCount) {
			throw new UnsupportedOperationException(ON_ITEM_RANGE_MOVED_EXCEPTION_MESSAGE);
		}

		@Override
		public final void onItemRangeRemoved(ObservableList<T> sender, int startPosition, int itemCount) {
			onListSizeChanged(itemCount);
		}
	}

	/**
	 * リスト内のある要素の位置が変更されたときに呼ばれるコールバック
	 * ある要素が一つ削除された直後に要素が一つ挿入されたとき、要素の位置が変更されたとみなす（削除された要素と挿入された要素の参照が等しいかどうかはチェックしていない）。
	 *
	 * @param <T> リスト要素の型
	 */
	public abstract static class OnItemMovedCallback<T> extends ObservableList.OnListChangedCallback<ObservableList<T>> {

		public abstract void onMoved(int fromPosition, int toPosition);

		private int fromPosition = -1;

		@Override
		public final void onChanged(ObservableList<T> sender) {
			fromPosition = -1;
		}

		@Override
		public final void onItemRangeChanged(ObservableList<T> sender, int positionStart, int itemCount) {
			fromPosition = -1;
		}

		@Override
		public final void onItemRangeInserted(ObservableList<T> sender, int positionStart, int itemCount) {
			if (itemCount == 1 && fromPosition != -1) {
				onMoved(fromPosition, positionStart);
			}

			fromPosition = -1;
		}

		@Override
		public final void onItemRangeMoved(ObservableList<T> sender, int fromPosition, int toPosition, int itemCount) {
			throw new UnsupportedOperationException(ON_ITEM_RANGE_MOVED_EXCEPTION_MESSAGE);
		}

		@Override
		public final void onItemRangeRemoved(ObservableList<T> sender, int positionStart, int itemCount) {
			fromPosition = itemCount == 1 ? positionStart : -1;
		}
	}

	private static final String ON_ITEM_RANGE_MOVED_EXCEPTION_MESSAGE = "java.util.List にも android.databinding.ObservableArrayList にもリストの要素を移動するようなメソッドは定義されておらず、実際 ObservableArrayList で onItemRangeMoved が呼ばれることもないため、このメソッドには対応していません。";
}