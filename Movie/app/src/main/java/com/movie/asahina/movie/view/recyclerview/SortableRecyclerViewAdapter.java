package com.movie.asahina.movie.view.recyclerview;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.function.Consumer;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.view.Utils.VariableLayoutPair;

import java.util.Collection;

/**
 * データバインディング対応 RecyclerViewAdapter。並べ替え対応。セクション分け非対応。
 * <p>
 * 参考： https://github.com/radzio/android-data-binding-recyclerview
 */
public class SortableRecyclerViewAdapter<T> extends RecyclerView.Adapter<SortableRecyclerViewAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {

    public SortableRecyclerViewAdapter(@NonNull Collection<T> itemCollection, @NonNull VariableLayoutPair variableLayoutPair, @IdRes int sortingHandlerViewId, @Nullable Consumer<T> onItemClick, @Nullable Consumer<T> onItemLongClick) {
        onListChangedCallback = new ObservableList.OnListChangedCallback<ObservableList<T>>() {

            @Override
            public void onChanged(ObservableList<T> sender) {
                notifyDataSetChanged();
            }

            @Override
            public void onItemRangeChanged(ObservableList<T> sender, int positionStart, int itemCount) {
                notifyItemRangeChanged(positionStart, itemCount);
            }

            @Override
            public void onItemRangeInserted(ObservableList<T> sender, int positionStart, int itemCount) {
                notifyItemRangeInserted(positionStart, itemCount);
            }

            @Override
            public void onItemRangeMoved(ObservableList<T> sender, int fromPosition, int toPosition, int itemCount) {
                notifyItemMoved(fromPosition, toPosition);
            }

            @Override
            public void onItemRangeRemoved(ObservableList<T> sender, int positionStart, int itemCount) {
                notifyItemRangeRemoved(positionStart, itemCount);
            }
        };

        this.variableLayoutPair = variableLayoutPair;

        this.onItemClick = onItemClick;

        this.onItemLongClick = onItemLongClick;

        if (itemCollection instanceof ObservableList) {
            itemList = (ObservableList<T>) itemCollection;
        } else {
            itemList = new ObservableArrayList<>();
            itemList.addAll(itemCollection);
        }
        notifyItemRangeInserted(0, itemList.size());
        itemList.addOnListChangedCallback(onListChangedCallback);

        //region 並べ替え機能の実装
        itemSortingHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0) {

            private int fromPosition = -1;

            private int toPosition = -1;

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int fromPosition = viewHolder.getAdapterPosition();
                int toPosition = target.getAdapterPosition();
                notifyItemMoved(fromPosition, toPosition);
                this.toPosition = toPosition;
                return true;
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);

                switch (actionState) {
                    case ItemTouchHelper.ACTION_STATE_DRAG:
                        fromPosition = viewHolder.getAdapterPosition();
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);

                if (fromPosition == toPosition || toPosition < 0) {
                    return;
                }

                itemList.removeOnListChangedCallback(onListChangedCallback);
                T item = itemList.remove(fromPosition);
                itemList.add(toPosition, item);
                toPosition = -1;
                itemList.addOnListChangedCallback(onListChangedCallback);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                // do nothing
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        });

        this.sortingHandlerViewId = sortingHandlerViewId;
        //endregion
    }

    private final ObservableList.OnListChangedCallback<ObservableList<T>> onListChangedCallback;

    private final ObservableList<T> itemList;

    private final VariableLayoutPair variableLayoutPair;

    @Nullable
    private final Consumer<T> onItemClick;

    @Nullable
    private final Consumer<T> onItemLongClick;

    private LayoutInflater inflater;

    //region 並べ替え機能の実装
    private final ItemTouchHelper itemSortingHelper;

    private final int sortingHandlerViewId;
    //endregion


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        //region 並べ替え機能の実装
        itemSortingHelper.attachToRecyclerView(recyclerView);
        //endregion
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        itemList.removeOnListChangedCallback(onListChangedCallback);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }

        return new ViewHolder(DataBindingUtil.inflate(inflater, viewType, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        T item = itemList.get(position);
        holder.binding.setVariable(variableLayoutPair.variableId, item);
        holder.binding.getRoot().setTag(R.id.key_sortable_recycler_view_adapter_item, item);

        if (onItemClick != null) {
            holder.binding.getRoot().setOnClickListener(this);
        }

        if (onItemLongClick != null) {
            holder.binding.getRoot().setOnLongClickListener(this);
        }

        //region 並べ替えの実装
        View sortingHandlerView = holder.binding.getRoot().findViewById(sortingHandlerViewId);
        if (sortingHandlerView != null) {
            sortingHandlerView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                        itemSortingHelper.startDrag(holder);
                        return true;
                    } else {
                        return false;
                    }
                }
            });
        }
        //endregion

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemViewType(int position) {
        return variableLayoutPair.layoutId;
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    @Override
    public void onClick(View v) {
        if (onItemClick == null) {
            return;
        }

        onItemClick.accept((T) v.getTag(R.id.key_sortable_recycler_view_adapter_item));
    }

    @Override
    public boolean onLongClick(View v) {
        if (onItemLongClick == null) {
            return false;
        }

        onItemLongClick.accept((T) v.getTag(R.id.key_sortable_recycler_view_adapter_item));
        return true;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        final ViewDataBinding binding;
    }
}