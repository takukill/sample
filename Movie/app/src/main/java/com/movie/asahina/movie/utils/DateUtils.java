package com.movie.asahina.movie.utils;

import android.text.TextUtils;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.format.DateTimeFormat;

/**
 * Created by takuasahina on 2017/12/12.
 */

public class DateUtils {

	public static DateTime dateFormat(String time) {
		return DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(time);
	}

	public static String dateItemDetail(String time) {
		if (TextUtils.isEmpty(time)) {
			return "1分前";
		}

		DateTime now = DateTime.now();

		Duration duration = new Duration(now, dateFormat(time));

		if (duration.getStandardDays() != 0) {
			return FormatUtils.formatNumber(String.valueOf(Math.abs(duration.getStandardDays()))) + "日前";
		}

		if (duration.getStandardHours() != 0) {
			return Math.abs(duration.getStandardHours()) + "時間前";
		}

		if (duration.getStandardMinutes() != 0) {
			return Math.abs(duration.getStandardMinutes()) + "分前";
		}

		return "1分前";
	}

	public static String negotiationDate(String time) {
		if (TextUtils.isEmpty(time)) {
			return "1分前";
		}

		DateTime now = DateTime.now();
		DateTime target = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(time);

		Duration duration = new Duration(now, target);

		if (target.getYear() == now.getYear()) {
			return FormatUtils.formatDate_MMDD(time);
		}

		if (target.getYear() != now.getYear()) {
			return FormatUtils.formatDate_YYYYMMDD_Slash(time);
		}

		if (duration.getStandardDays() != 0) {
			return FormatUtils.formatNumber(String.valueOf(Math.abs(duration.getStandardDays()))) + "日前";
		}

		if (duration.getStandardHours() != 0) {
			return Math.abs(duration.getStandardHours()) + "時間前";
		}

		if (duration.getStandardMinutes() != 0) {
			return Math.abs(duration.getStandardMinutes()) + "分前";
		}

		return "1分前";
	}

	public static String date_HHMM(String time) {
		if (TextUtils.isEmpty(time)) {
			return "";
		}

		DateTime target = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(time);
		return target.toString("H:mm");
	}

	public static String date_YYYYMMDD_HHMM(String time) {
		if (TextUtils.isEmpty(time)) {
			return "";
		}

		DateTime target = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(time);

		return target.toString("yyyy/M/d HH:mm");
	}

	public static boolean compareToDate(String date1, String date2) {
		DateTime dateTime1 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(date1);
		DateTime dateTime2 = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime(date2);

		if (dateTime1.getYear() != dateTime2.getYear()) {
			return false;
		}

		if (dateTime1.getMonthOfYear() != dateTime2.getMonthOfYear()) {
			return false;
		}

		if (dateTime1.getDayOfMonth() != dateTime2.getDayOfMonth()) {
			return false;
		}

		return true;
	}
}
