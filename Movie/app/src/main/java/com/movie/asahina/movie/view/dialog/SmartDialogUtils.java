package com.movie.asahina.movie.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Window;

import com.annimon.stream.Collectors;
import com.annimon.stream.IntPair;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.movie.asahina.movie.BR;
import com.movie.asahina.movie.BaseApplication;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.databinding.DialogSingleChoiceFragmentBinding;
import com.movie.asahina.movie.view.recyclerview.DataBindingRecyclerViewUtils;

import java.util.List;

/**
 * Created by takuasahina on 2017/12/08.
 */

public class SmartDialogUtils {

//	public static <T> Dialog showDialog(Context context, T itemViewModel, int variableId, @LayoutRes int layoutId) {
//
//		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//		ViewDataBinding dialogBinding = DataBindingUtil.inflate(inflater, layoutId, null, false);
//		dialogBinding.setVariable(variableId, itemViewModel);
//
//		Dialog dialog = new Dialog(context, R.style.Dialog_Style);
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		dialog.setContentView(dialogBinding.getRoot());
//		return dialog;
//	}
//
//
//	public static class ReportDialog {
//
//		public static void show(Context context, final Consumer<Pair<ReportCodeTypeEnum, String>> onClickPositiveButton, final Consumer<Pair<ReportCodeTypeEnum, String>> onClickNegativeButton) {
//
//			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//			ViewDataBinding dialogBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_report, null, false);
//			showReportDialogViewModel = new ShowReportDialogViewModel(new ShowReportDialogViewModel.Delegate() {
//				@Override
//				public void selectPositive() {
//					onClickPositiveButton.accept(new Pair<ReportCodeTypeEnum, String>(showReportDialogViewModel.reportCodeTypeEnum.get(), showReportDialogViewModel.reportContent.get()));
//					reportDialog.dismiss();
//				}
//
//				@Override
//				public void selectNegative() {
//					onClickNegativeButton.accept(new Pair<ReportCodeTypeEnum, String>(showReportDialogViewModel.reportCodeTypeEnum.get(), showReportDialogViewModel.reportContent.get()));
//					reportDialog.dismiss();
//				}
//			});
//			dialogBinding.setVariable(BR.viewModel, showReportDialogViewModel);
//
//			reportDialog = new Dialog(context, R.style.Dialog_Style);
//			reportDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//			reportDialog.setContentView(dialogBinding.getRoot());
//			reportDialog.show();
//		}
//
//
//		private static Dialog reportDialog;
//
//		private static ShowReportDialogViewModel showReportDialogViewModel;
//
//
//		public static class ShowReportDialogViewModel {
//
//
//			public ShowReportDialogViewModel(Delegate delegate) {
//				this.delegate = delegate;
//			}
//
//
//			public ObservableField<ReportCodeTypeEnum> reportCodeTypeEnum = new ObservableField<>(ReportCodeTypeEnum.SPAM);
//
//			public ObservableField<String> reportContent = new ObservableField<>();
//
//
//			public void selectSpam() {
//				reportCodeTypeEnum.set(ReportCodeTypeEnum.SPAM);
//			}
//
//			public void selectEncounter() {
//				reportCodeTypeEnum.set(ReportCodeTypeEnum.ENCOUNTER);
//			}
//
//			public void selectNuisances() {
//				reportCodeTypeEnum.set(ReportCodeTypeEnum.NUISANCES);
//			}
//
//			public void selectOther() {
//				reportCodeTypeEnum.set(ReportCodeTypeEnum.OTHER);
//			}
//
//			public void selectPositive() {
//				delegate.selectPositive();
//			}
//
//			public void selectNegative() {
//				delegate.selectNegative();
//			}
//
//
//			private final Delegate delegate;
//
//			public interface Delegate {
//
//				void selectPositive();
//
//				void selectNegative();
//
//			}
//		}
//	}
//
//
//	public static class SimpleDialog {
//
//		public static void show(Activity activity, String title, String message, boolean cancelable, Integer positiveColorInt, String textPositiveButton, Runnable onClickPositiveButton, String textNegativeButton, Runnable onClickNegativeButton, boolean dismissOnPause, Runnable onDismissRunnable) {
//			LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			SmartAlertDialogLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.smart_alert_dialog_layout, null, false);
//
//			int positiveColor;
//			if (positiveColorInt != null) {
//				positiveColor = activity.getResources().getColor(positiveColorInt);
//			} else {
//				positiveColor = activity.getResources().getColor(R.color.greyish_brown_444444);
//			}
//
//			int negativeColor = activity.getResources().getColor(R.color.greyish_brown_444444);
//
//			SimpleDialogViewModel viewModel = new SimpleDialogViewModel(title, message, positiveColor, textPositiveButton, onClickPositiveButton, negativeColor, textNegativeButton, onClickNegativeButton, dismissOnPause, onDismissRunnable,
//					new SimpleDialogViewModel.Delegate() {
//						@Override
//						public void selectPositive() {
//							dialog.dismiss();
//						}
//
//						@Override
//						public void selectNegative() {
//							dialog.dismiss();
//						}
//					});
//			binding.setViewModel(viewModel);
//
//			dialog = new Dialog(activity, R.style.Dialog_Style);
//			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//			dialog.setContentView(binding.getRoot());
//			dialog.setCancelable(cancelable);
//			dialog.show();
//		}
//
//		public static void show(String title, String message, boolean cancelable, Integer positiveColorInt, String textPositiveButton, Runnable onClickPositiveButton, String textNegativeButton, Runnable onClickNegativeButton, boolean dismissOnPause, Runnable onDismissRunnable) {
//			LayoutInflater inflater = (LayoutInflater) BaseApplication.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			SmartAlertDialogLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.smart_alert_dialog_layout, null, false);
//
//			int positiveColor;
//			if (positiveColorInt != null) {
//				positiveColor = BaseApplication.getContext().getResources().getColor(positiveColorInt);
//			} else {
//				positiveColor = BaseApplication.getContext().getResources().getColor(R.color.greyish_brown_444444);
//			}
//
//			int negativeColor = BaseApplication.getContext().getResources().getColor(R.color.greyish_brown_444444);
//
//			SimpleDialogViewModel viewModel = new SimpleDialogViewModel(title, message, positiveColor, textPositiveButton, onClickPositiveButton, negativeColor, textNegativeButton, onClickNegativeButton, dismissOnPause, onDismissRunnable,
//					new SimpleDialogViewModel.Delegate() {
//						@Override
//						public void selectPositive() {
//							dialog.dismiss();
//						}
//
//						@Override
//						public void selectNegative() {
//							dialog.dismiss();
//						}
//					});
//			binding.setViewModel(viewModel);
//
//			dialog = new Dialog(BaseApplication.getContext(), R.style.Dialog_Style);
//			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//			dialog.setContentView(binding.getRoot());
//			dialog.setCancelable(cancelable);
//			dialog.show();
//		}
//
//		public static void show(Activity activity, String title, String message, boolean cancelable, String textPositiveButton, Runnable onClickPositiveButton, String textNegativeButton, Runnable onClickNegativeButton) {
//			show(activity, title, message, cancelable, null, textPositiveButton, onClickPositiveButton, textNegativeButton, onClickNegativeButton, false, null);
//		}
//
//		public static void show(Activity activity, String title, String message, boolean cancelable, String textPositiveButton, Runnable onClickPositiveButton) {
//			show(activity, title, message, cancelable, null, textPositiveButton, onClickPositiveButton, null, null, false, null);
//		}
//
//		public static void show(Activity activity, String title, String message, boolean cancelable, Integer positiveColor, String textPositiveButton, Runnable onClickPositiveButton, String textNegativeButton, Runnable onClickNegativeButton) {
//			show(activity, title, message, cancelable, positiveColor, textPositiveButton, onClickPositiveButton, textNegativeButton, onClickNegativeButton, false, null);
//		}
//
//		public static void show(Activity activity, String title, String message, boolean cancelable, Integer positiveColor, String textPositiveButton, Runnable onClickPositiveButton) {
//			show(activity, title, message, cancelable, positiveColor, textPositiveButton, onClickPositiveButton, null, null, false, null);
//		}
//
//		public static void show(String title, String message, boolean cancelable, Integer positiveColor, String textPositiveButton, Runnable onClickPositiveButton) {
//			show(title, message, cancelable, positiveColor, textPositiveButton, onClickPositiveButton, null, null, false, null);
//		}
//
//		private static Dialog dialog;
//
//		public static class SimpleDialogViewModel {
//
//			public SimpleDialogViewModel(String title, String message, int positiveColor, String textPositiveButton, Runnable onClickPositiveButton, int negativeColor, String textNegativeButton, Runnable onClickNegativeButton, boolean dismissOnPause, Runnable onDismissRunnable, Delegate delegate) {
//				this.delegate = delegate;
//				this.title = title;
//				this.message = message;
//				this.positiveColor = positiveColor;
//				this.textPositiveButton = textPositiveButton;
//				this.onClickPositiveButton = onClickPositiveButton;
//				this.negativeColor = negativeColor;
//				this.textNegativeButton = textNegativeButton;
//				this.onClickNegativeButton = onClickNegativeButton;
//				this.dismissOnPause = dismissOnPause;
//				this.onDismissRunnable = onDismissRunnable;
//			}
//
//
//			public final String title;
//
//			public final String message;
//
//			public final int positiveColor;
//
//			public final String textPositiveButton;
//
//			public final Runnable onClickPositiveButton;
//
//			public final int negativeColor;
//
//			public final String textNegativeButton;
//
//			public final Runnable onClickNegativeButton;
//
//			public final boolean dismissOnPause;
//
//			public final Runnable onDismissRunnable;
//
//
//			public void selectPositive() {
//				if (onClickPositiveButton == null) {
//					return;
//				}
//
//				onClickPositiveButton.run();
//				delegate.selectPositive();
//			}
//
//			public void selectNegative() {
//				if (onClickNegativeButton == null) {
//					return;
//				}
//
//				onClickNegativeButton.run();
//				delegate.selectNegative();
//			}
//
//
//			private Delegate delegate;
//
//			public interface Delegate {
//
//				void selectPositive();
//
//				void selectNegative();
//			}
//		}
//	}

//	/**
//	 * 列挙体とラベルのペアのリストとコールバックからなる単一選択ダイアログ
//	 */
//	public static <T> void showSingleChoiceDialog(AppCompatActivity activity, String title, int variableId, @LayoutRes int layoutId, List<T> itemLabelPairList, ObservableField<T> checkedItem, Consumer<T> onSelected) {
//		SingleChoiceDialogFragment.showSuccessDialog(activity.getSupportFragmentManager(), title, variableId, layoutId, itemLabelPairList, checkedItem, onSelected);
//	}
//
//
//	public static <T> void showSingleChoiceDialog(AppCompatActivity activity, String title, List<T> itemList, ObservableField<T> item, Consumer<T> consumer) {
//		SingleChoiceDialogFragment.showSuccessDialog(activity.getSupportFragmentManager(), title, itemList, item, consumer);
//	}
//
//
//	/**
//	 * 列挙体とラベルのペアのリストとコールバックからなる単一選択ダイアログの DialogFragment。
//	 */
//	public static class SingleChoiceDialogFragment2<T> extends DialogFragment {
//
//		private static final String KEY_TITLE = "KEY_TITLE";
//
//		private static final String KEY_ITEM_LIST_JSON = "KEY_ITEM_LIST_JSON";
//
//		private static final String KEY_CHECKED_ITEM = "KEY_CHECKED_ITEM";
//
//		private static final String KEY_ITEM_LAYOUT = "KEY_ITEM_LAYOUT";
//
////		public static <T> void showSuccessDialog(FragmentManager fragmentManager, String title, int variableId, int layoutId, List<T> itemList, ObservableField<T> checkedItem, Consumer<T> onSelected) {
////			SingleChoiceDialogFragment dialogFragment = new SingleChoiceDialogFragment();
////
////			Bundle args = new Bundle();
////			args.putString(KEY_TITLE, title);
////			args.putString(KEY_ITEM_LIST_JSON, new Gson().toJson(itemList));
////			args.putString(KEY_ITEM_LAYOUT, new Gson().toJson(new VariableLayoutPair(variableId, layoutId)));
////
////			args.putSerializable(KEY_CHECKED_ITEM, checkedItem);
////			dialogFragment.setArguments(args);
////
////			dialogFragment.onSelected = onSelected;
////
////			dialogFragment.show(fragmentManager, AlertDialogs.EnumItemSingleChoiceDialogFragment.class.getSimpleName());
////		}
//
//		public static <T> void showSuccessDialog(FragmentManager fragmentManager, String title, List<T> itemList, ObservableField<T> item, Consumer<T> onSelected) {
//			SingleChoiceDialogFragment dialogFragment = new SingleChoiceDialogFragment();
//
//			Bundle args = new Bundle();
//			args.putString(KEY_TITLE, title);
//			args.putString(KEY_ITEM_LIST_JSON, new Gson().toJson(itemList));
//
//			args.putSerializable(KEY_CHECKED_ITEM, item);
//			dialogFragment.setArguments(args);
//
//			dialogFragment.onSelected = onSelected;
//
//			dialogFragment.show(fragmentManager, AlertDialogs.EnumItemSingleChoiceDialogFragment.class.getSimpleName());
//		}
//
//		private boolean recreated;
//
//		private Consumer<T> onSelected;
//
//		private Dialog dialog;
//
//		public String title;
//
//		@NonNull
//		@Override
//		public Dialog onCreateDialog(Bundle savedInstanceState) {
//			recreated = savedInstanceState != null;
//
//			LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			DialogSingleChoiseFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_single_choise_fragment, null, false);
//			binding.setView(this);
//
//			recreated = savedInstanceState != null;
//
//			title = getArguments().getString(KEY_TITLE);
//
//			List<T> itemList = new Gson().fromJson(getArguments().getString(KEY_ITEM_LIST_JSON), new TypeToken<List<T>>() {
//			}.getType());
//
////			VariableLayoutPair variableLayoutPair = new Gson().fromJson(getArguments().getString(KEY_ITEM_LAYOUT), new TypeToken<VariableLayoutPair>() {
////			}.getType());
//
//			final ObservableField<T> selectedItem = (ObservableField<T>) getArguments().getSerializable(KEY_CHECKED_ITEM);
//
//			DataBindingRecyclerViewUtils.bind(binding.recyclerView, itemList, BR.baseViewModel, , new Consumer<Pair<Integer, T>>() {
//				@Override
//				public void accept(Pair<Integer, T> itemViewModel) {
//					selectedItem.set(itemViewModel.second);
//					dialog.dismiss();
//				}
//			});
//
//			// 選択中のアイテムが見える位置まで移動する（可能な場合に、 2 つ、または 1 つ前のアイテムが先頭になるように移動しているが、これは選択中のアイテムよりも前にアイテムが存在することをユーザーに伝えるため。DatePickerDialog の「年」選択と同じ動き）
//			binding.recyclerView.scrollToPosition(Stream.of(itemList)
//					.indexed()
//					.filter(new Predicate<IntPair<T>>() {
//						@Override
//						public boolean test(IntPair<T> value) {
//							return value.getSecond() == selectedItem.get();
//						}
//					})
//					.map(new Function<IntPair<T>, Integer>() {
//						@Override
//						public Integer apply(IntPair<T> pair) {
//							int selectedItemIndex = pair.getFirst();
//							return 2 <= selectedItemIndex
//									? selectedItemIndex - 2
//									: 0;
//						}
//					})
//					.findFirst()
//					.orElse(0));
//
//			dialog = new Dialog(getActivity());
//			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//
//			return dialog;
//		}
//
//		@Override
//		public void onResume() {
//			super.onResume();
//
//			if (recreated) {
//				dismiss();
//			}
//		}
//	}


	public static class SingleChoiceSimpleDialog {

		public static void show(AppCompatActivity activity, String title, List<String> itemList, int position, Consumer<Pair<Integer, String>> consumer) {
			SingleChoiceDialogFragment.showDialog(activity.getSupportFragmentManager(), title, itemList, position, consumer);
		}

		public static void show(Fragment fragment, String title, List<String> itemList, int position, Consumer<Pair<Integer, String>> consumer) {
			SingleChoiceDialogFragment.showDialog(fragment.getFragmentManager(), title, itemList, position, consumer);
		}

		/**
		 * 列挙体とラベルのペアのリストとコールバックからなる単一選択ダイアログの DialogFragment。
		 */
		public static class SingleChoiceDialogFragment extends DialogFragment {

			private static final String KEY_TITLE = "KEY_TITLE";

			private static final String KEY_ITEM_LIST_JSON = "KEY_ITEM_LIST_JSON";

			private static final String KEY_CHECKED_ITEM = "KEY_CHECKED_ITEM";

			public static void showDialog(FragmentManager fragmentManager, String title, List<String> itemList, int position, Consumer<Pair<Integer, String>> onSelected) {
				SingleChoiceDialogFragment dialogFragment = new SingleChoiceDialogFragment();

				Bundle args = new Bundle();
				args.putString(KEY_TITLE, title);
				args.putString(KEY_ITEM_LIST_JSON, new Gson().toJson(itemList));
				args.putInt(KEY_CHECKED_ITEM, position);
				dialogFragment.setArguments(args);

				dialogFragment.onSelected = onSelected;

				dialogFragment.show(fragmentManager, "Single");
			}

			private boolean recreated;

			private Consumer<Pair<Integer, String>> onSelected;

			private Dialog dialog;

			public ObservableField<String> title = new ObservableField<>();

			@NonNull
			@Override
			public Dialog onCreateDialog(Bundle savedInstanceState) {
				recreated = savedInstanceState != null;

				LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				DialogSingleChoiceFragmentBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_single_choice_fragment, null, false);
				binding.setView(this);

				recreated = savedInstanceState != null;

				title.set(getArguments().getString(KEY_TITLE));

				final ObservableList<String> itemList = new ObservableArrayList<>();
				itemList.addAll((List<String>) new Gson().fromJson(getArguments().getString(KEY_ITEM_LIST_JSON), new TypeToken<List<String>>() {
				}.getType()));

				final int position = getArguments().getInt(KEY_CHECKED_ITEM);

				final List<SimpleSingleChoiceItemViewModel> simpleList = Stream.of(itemList)
						.indexed()
						.map(new Function<IntPair<String>, SimpleSingleChoiceItemViewModel>() {
							@Override
							public SimpleSingleChoiceItemViewModel apply(IntPair<String> stringIntPair) {
								return new SimpleSingleChoiceItemViewModel(position == stringIntPair.getFirst(), stringIntPair.getSecond());
							}
						})
						.collect(Collectors.<SimpleSingleChoiceItemViewModel>toList());

				DataBindingRecyclerViewUtils.bind(binding.recyclerView, simpleList, BR.viewModel, R.layout.item_simple_choise_dialog, new Consumer<Pair<Integer, SimpleSingleChoiceItemViewModel>>() {
					@Override
					public void accept(final Pair<Integer, SimpleSingleChoiceItemViewModel> pair) {
						Stream.of(simpleList)
								.forEach(new Consumer<SimpleSingleChoiceItemViewModel>() {
									@Override
									public void accept(SimpleSingleChoiceItemViewModel item) {
										item.check.set(pair.second == item);
									}
								});
						onSelected.accept(new Pair<Integer, String>(pair.first, pair.second.title));
						dialog.dismiss();
					}
				});

				binding.recyclerView.scrollToPosition(position == 0 ? 0 : position - 1);

				dialog = new Dialog(getActivity());
				dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(binding.getRoot());

				return dialog;
			}

			@Override
			public void onResume() {
				super.onResume();
				if (recreated) {
					dismiss();
				}
			}

			public static class SimpleSingleChoiceItemViewModel {

				public SimpleSingleChoiceItemViewModel(boolean check, String title) {
					this.check.set(check);
					this.title = title;
				}


				public final ObservableBoolean check = new ObservableBoolean();

				public final String title;
			}
		}
	}
}
