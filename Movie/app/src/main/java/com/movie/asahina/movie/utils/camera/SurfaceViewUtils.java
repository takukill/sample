package com.movie.asahina.movie.utils.camera;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Timer;

/**
 * Created by takuasahina on 2017/11/07.
 */

public class SurfaceViewUtils implements SurfaceHolder.Callback, Camera.PreviewCallback {

	private final Handler handler;
	private byte[] previewData;

	public SurfaceViewUtils(Activity activity) {
		this.activity = activity;

		this.timer = new Timer();

		this.handler = new Handler();
	}


	private final Activity activity;

	private final Timer timer;

	private Camera camera;

	private SurfaceHolder holder;

	private boolean shutterFlag;

	private boolean focusFlag;

	private CameraUtils.CameraListener cameraListener = null;

	private CameraUtils.QRReader qrReader = null;


	public void openCamera(SurfaceView surfaceView) {
		holder = surfaceView.getHolder();

		holder.addCallback(this);
		holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		surfaceView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (focusFlag) {
					return false;
				}

				focusFlag = true;

				if (camera != null && event.getAction() == MotionEvent.ACTION_DOWN) {
					camera.autoFocus(new Camera.AutoFocusCallback() {
						@Override
						public void onAutoFocus(boolean success, Camera camera) {
							focusFlag = false;
						}
					});
				}
				return false;
			}
		});
	}

	public void closeCamera() {
		if (timer != null) {
			timer.cancel();
		}

		if (handler != null) {
			handler.removeCallbacksAndMessages(null);
		}

		if (camera != null) {
			setTorchMode(false);
			surfaceDestroyed(holder);
		}
	}

	public void setCameraListener(CameraUtils.CameraListener cameraListener) {
		this.cameraListener = cameraListener;
	}

	public void setQRReader(final CameraUtils.QRReader qrReader) {
		this.qrReader = qrReader;
	}


	public void takePicture(int layoutWidth, int layoutHeight, int x, int y, final int viewWidth, final int viewHeight) {

		if (cameraListener == null || shutterFlag == true) {
			return;
		}

		shutterFlag = true;

		camera.takePicture(null, null, new Camera.PictureCallback() {
			@Override
			public void onPictureTaken(byte[] bytes, Camera camera) {

				camera.startPreview();

				Bitmap byteBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);

				BigDecimal widthScale = new BigDecimal(byteBitmap.getHeight()).divide(new BigDecimal(layoutWidth), 4, BigDecimal.ROUND_DOWN);
				BigDecimal heightScale = new BigDecimal(byteBitmap.getWidth()).divide(new BigDecimal(layoutHeight), 4, BigDecimal.ROUND_DOWN);

				int bitmapX = new BigDecimal(x).multiply(widthScale).intValue();
				int bitmapY = new BigDecimal(y).multiply(heightScale).intValue();
				int bitmapWidth = new BigDecimal(viewWidth).multiply(widthScale).intValue();
				int bitmapHeight = new BigDecimal(viewHeight).multiply(heightScale).intValue();

				Matrix matrix = new Matrix();
				matrix.setRotate(90);

				Bitmap bitmap = Bitmap.createBitmap(byteBitmap, bitmapY, bitmapX, bitmapHeight, bitmapWidth, matrix, true);
				cameraListener.onCameraTakePicture(Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), new BigDecimal(bitmap.getWidth()).multiply(new BigDecimal(viewHeight).divide(new BigDecimal(viewWidth), 4, BigDecimal.ROUND_DOWN)).intValue(), false));

				shutterFlag = false;
			}
		});
	}

	public void setTorchMode(boolean on) {
		Camera.Parameters camParam = camera.getParameters();
		if (on) {
			camParam.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
		} else {
			camParam.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
		}

		//パラメータを設定
		camera.setParameters(camParam);
	}


	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// カメラインスタンスを取得
		camera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);

		Camera.Parameters param = camera.getParameters();
		param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
		camera.setParameters(param);

		camera.setPreviewCallback(this);

		try {
			int d = 90;
			camera.setDisplayOrientation(d);
			camera.setPreviewDisplay(holder);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	synchronized public void surfaceDestroyed(SurfaceHolder holder) {
		// カメラインスタンス開放
		if (camera != null) {
			holder.removeCallback(this);
			camera.stopPreview();
			camera.setPreviewCallback(null);
			camera.release();
			camera = null;
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		Camera.Parameters parameters = camera.getParameters();
		// 指定したサイズ(width,height)に最も近いプレビュー用映像サイズを選択する
		final Camera.Size closestSize = getClosestSupportedSize(parameters.getSupportedPreviewSizes(), width, height);
		parameters.setPreviewSize(closestSize.width, closestSize.height);
		// 指定したサイズ(width,height)に最も近い静止画撮影用映像サイズを選択する
		final Camera.Size pictureSize = getClosestSupportedSize(parameters.getSupportedPictureSizes(), width, height);
		parameters.setPictureSize(pictureSize.width, pictureSize.height);
		camera.setParameters(parameters);
		camera.startPreview();
	}

	private static Camera.Size getClosestSupportedSize(final List<Camera.Size> supportedSizes, final int requestedWidth, final int requestedHeight) {
		return (Camera.Size) Collections.min(supportedSizes, new Comparator<Camera.Size>() {

			private int diff(final Camera.Size size) {
				return Math.abs(requestedWidth - size.width) + Math.abs(requestedHeight - size.height);
			}

			@Override
			public int compare(final Camera.Size lhs, final Camera.Size rhs) {
				return diff(lhs) - diff(rhs);
			}
		});
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		previewData = data;
//		readQRCode(data);
	}

//	public void readQRCode(byte[] data) {
//		if (qrReader != null) {
//			// 読み込む範囲
//			int previewWidth = camera.getParameters().getPreviewSize().width;
//			int previewHeight = camera.getParameters().getPreviewSize().height;
//
//			// プレビューデータから BinaryBitmap を生成
//			PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(data, previewWidth, previewHeight, 0, 0, previewWidth, previewHeight, false);
//			BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
//			try {
//				String result = new MultiFormatReader().decode(bitmap).getText();
//				if (!TextUtils.isEmpty(result)) {
//					qrReader.onReadQRCode(result);
//				}
//			} catch (Exception e) {
//
//			}
//		}
//	}
}
