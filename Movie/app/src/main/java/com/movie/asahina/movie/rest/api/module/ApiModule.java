package com.movie.asahina.movie.rest.api.module;

import com.movie.asahina.movie.rest.api.AccountPostApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger2で注入するオブジェクトを指定する。
 */
@Module
public class ApiModule {

	@Provides
	@Singleton
	public AccountPostApi provideAccountPostApi() {
		return new AccountPostApi();
	}
}
