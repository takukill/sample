package com.movie.asahina.movie.view.link;

/**
 * Created by takuasahina on 2018/01/30.
 */

import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.MotionEvent;
import android.widget.TextView;

public class ChatLinkMovementMethod extends android.text.method.LinkMovementMethod {

	private static ChatLinkMovementMethod instance;

	public static ChatLinkMovementMethod getInstance(OnClickLink onClickLink) {
		if (instance == null) {
			instance = new ChatLinkMovementMethod();
			instance.setDelegate(onClickLink);
		}

		return instance;
	}

	@Override
	public boolean onTouchEvent(TextView widget, Spannable buffer, MotionEvent event) {
		int action = event.getAction();

		if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_DOWN) {

			int x = (int) event.getX();
			int y = (int) event.getY();

			x -= widget.getTotalPaddingLeft();
			y -= widget.getTotalPaddingTop();

			x += widget.getScrollX();
			y += widget.getScrollY();

			Layout layout = widget.getLayout();
			int line = layout.getLineForVertical(y);
			int off = layout.getOffsetForHorizontal(line, x);

			ClickableSpan[] link = buffer.getSpans(off, off, ClickableSpan.class);

			if (link.length == 0) {

				Selection.removeSelection(buffer);

			} else {

				switch (action) {
					case MotionEvent.ACTION_UP:
						if (link[0] instanceof URLSpan && delegate != null) {
							String url = ((URLSpan) link[0]).getURL();
							delegate.selectLink(url);
						} else {
							link[0].onClick(widget);
						}
						break;

					case MotionEvent.ACTION_DOWN:
						break;
				}

				return false;
			}
		}

		return false;
	}


	public void setDelegate(OnClickLink delegate) {
		this.delegate = delegate;
	}


	private OnClickLink delegate;

	public interface OnClickLink {

		void selectLink(String url);

	}
}