package com.movie.asahina.movie;


import com.movie.asahina.movie.model.module.ModelModule;
import com.movie.asahina.movie.preferences.PreferencesModule;
import com.movie.asahina.movie.rest.api.module.ApiModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
		ModelModule.class,
        PreferencesModule.class,
        ApiModule.class
})
public interface BaseComponent {

    // Model
//    void inject(MasterModel model);

    // ViewModel
//    void inject(StartUpViewModel viewModel);
}
