package com.movie.asahina.movie.rest.base;

import android.databinding.ObservableArrayMap;
import android.support.annotation.NonNull;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;

import java.util.List;

/**
 * レスポンス共通項目
 */

public class CommonResponse {

	/**
	 * コード
	 */
	public String code;

	/**
	 * 開発者向けメッセージ
	 */
//	public MessageTypeEnum message;

	/**
	 * ヘッダとボディのどちらに問題があったかの情報
	 */
	public String payload_section;

	/**
	 * エラーの詳細内容
	 */
	public String user_message_detail;

	/**
	 * エラーメッセージ部
	 */
	public List<ErrorMessageText> errors;

	/**
	 * エラーメッセージ部から、エラーメッセージを整形して返す。
	 *
	 * @return
	 */
	@NonNull
	public ObservableArrayMap<String, String> getErrorMessage() {

		final ObservableArrayMap<String, String> errorMap = new ObservableArrayMap<>();

		Stream.of(errors)
				.forEach(new Consumer<ErrorMessageText>() {
					@Override
					public void accept(ErrorMessageText errorMessageText) {
						errorMap.put(errorMessageText.field, errorMessageText.user_message_detail);
					}
				});

		return errorMap;
	}

	public class ErrorMessageText {

		/**
		 * 問題となった項目名
		 */
		public String field;

		/**
		 * エラーメッセージ
		 */
		public String user_message_detail;
	}
}
