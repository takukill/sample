package com.movie.asahina.movie.view.recyclerview;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.IntPair;
import com.annimon.stream.IntStream;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.IntSupplier;
import com.annimon.stream.function.IntUnaryOperator;
import com.annimon.stream.function.ToIntFunction;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.view.Utils.VariableLayoutPair;

import java.util.List;

/**
 * データバインディング対応 RecyclerViewAdapter。セクション分け対応。
 * <p>
 * 参考： https://github.com/radzio/android-data-binding-recyclerview
 */
public class SectionedRecyclerViewAdapter extends RecyclerView.Adapter<SectionedRecyclerViewAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener {

    public SectionedRecyclerViewAdapter(@NonNull List<Section> sectionList) {
        this.sectionList = sectionList;

        int totalSize = Stream.of(sectionList)
                .mapToInt(new ToIntFunction<Section>() {
                    @Override
                    public int applyAsInt(Section section) {
                        return section.getSize();
                    }
                })
                .sum();

        notifyItemRangeInserted(0, totalSize);

        Stream.of(sectionList)
                .indexed()
                .forEach(new Consumer<IntPair<Section>>() {
                    @Override
                    public void accept(final IntPair<Section> indexSectionPair) {
                        indexSectionPair.getSecond().setAdapter(SectionedRecyclerViewAdapter.this);

                        indexSectionPair.getSecond().setSubheaderPositionSupplier(new IntSupplier() {
                            @Override
                            public int getAsInt() {
                                return IntStream.range(0, indexSectionPair.getFirst())
                                        .map(new IntUnaryOperator() {
                                            @Override
                                            public int applyAsInt(int i) {
                                                return SectionedRecyclerViewAdapter.this.sectionList.get(i).getSize();
                                            }
                                        })
                                        .sum();
                            }
                        });

                        indexSectionPair.getSecond().addOnSubheaderChangedCallback();
                        indexSectionPair.getSecond().addOnItemListChangedCallback();
                    }
                });
    }

    private final List<Section> sectionList;

    private LayoutInflater inflater;

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        //region 並べ替えの実装（SortableSection のみ）
        for (Section section : sectionList) {
            if (section instanceof SortableSection) {
                ((SortableSection) section).attachItemSortingHelperToRecyclerView(recyclerView);
            }
        }
        //endregion
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        for (Section section : sectionList) {
            section.removeOnItemListChangedCallback();
            section.removeOnSubheaderChangedCallback();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }

        return new ViewHolder(DataBindingUtil.inflate(inflater, viewType, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        for (int i = 0; i < sectionList.size(); i++) {
            Section section = sectionList.get(i);
            if (section.getSize() == 0) {
                continue;
            }

            int subheaderPosition = IntStream.range(0, i)
                    .map(new IntUnaryOperator() {
                        @Override
                        public int applyAsInt(int j) {
                            return sectionList.get(j).getSize();
                        }
                    })
                    .sum();

            if (subheaderPosition == position) {
                holder.binding.setVariable(section.getSubheaderVariableLayoutPair().variableId, section.getSubheader().get());
                holder.binding.executePendingBindings();
                break;
            } else if (subheaderPosition < position && position < subheaderPosition + section.getSize()) {
                Object item = section.getItemList().get(position - (subheaderPosition + 1));
                holder.binding.setVariable(section.getItemVariableLayoutPair().variableId, item);
                holder.binding.getRoot().setTag(R.id.key_sectioned_recycler_view_adapter_item, item);
                holder.binding.getRoot().setTag(R.id.key_simple_recycler_view_adapter_position, position);
                holder.binding.getRoot().setTag(R.id.key_sectioned_recycler_view_adapter_section_index, i);

                if (section.getOnItemClick() != null) {
                    holder.binding.getRoot().setOnClickListener(this);
                } else {
                    holder.binding.getRoot().setOnClickListener(null);
                    holder.binding.getRoot().setClickable(false);
                }

                if (section.getOnItemLongClick() != null) {
                    holder.binding.getRoot().setOnLongClickListener(this);
                } else {
                    holder.binding.getRoot().setOnLongClickListener(null);
                    holder.binding.getRoot().setLongClickable(false);
                }

                //region 並べ替えの実装（SortableSection のみ）
                if (section instanceof SortableSection) {
                    final SortableSection sortableSection = (SortableSection) section;
                    View sortingHandlerView = holder.binding.getRoot().findViewById(sortableSection.sortingHandlerViewId);
                    if (sortingHandlerView != null) {
                        sortingHandlerView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                                    sortableSection.startSorting(holder);
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                        });
                    }
                }
                //endregion

                holder.binding.executePendingBindings();
                break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        for (int i = 0; i < sectionList.size(); i++) {
            Section section = sectionList.get(i);
            if (section.getSize() == 0) {
                continue;
            }

            int subheaderPosition = IntStream.range(0, i)
                    .map(new IntUnaryOperator() {
                        @Override
                        public int applyAsInt(int j) {
                            return sectionList.get(j).getSize();
                        }
                    })
                    .sum();

            if (subheaderPosition == position) {
                return section.getSubheaderVariableLayoutPair().layoutId;
            } else if (subheaderPosition < position && position < subheaderPosition + section.getSize()) {
                return section.getItemVariableLayoutPair().layoutId;
            }
        }

        throw new IllegalStateException();
    }

    @Override
    public int getItemCount() {
        return Stream.of(sectionList)
                .mapToInt(new ToIntFunction<Section>() {
                    @Override
                    public int applyAsInt(Section section) {
                        return section.getSize();
                    }
                })
                .sum();
    }

    @Override
    public void onClick(View v) {
        Integer sectionIndex = (Integer) v.getTag(R.id.key_sectioned_recycler_view_adapter_section_index);
        if (sectionIndex == null) {
            return;
        }

        Consumer onItemClick = sectionList.get(sectionIndex).getOnItemClick();
        if (onItemClick == null) {
            return;
        }

        onItemClick.accept(new Pair<>((Integer) v.getTag(R.id.key_simple_recycler_view_adapter_position), v.getTag(R.id.key_sectioned_recycler_view_adapter_item)));
    }

    @Override
    public boolean onLongClick(View v) {
        Integer sectionIndex = (Integer) v.getTag(R.id.key_sectioned_recycler_view_adapter_section_index);
        if (sectionIndex == null) {
            return false;
        }

        Consumer onItemLongClick = sectionList.get(sectionIndex).getOnItemLongClick();
        if (onItemLongClick == null) {
            return false;
        }

        onItemLongClick.accept(new Pair<>((Integer) v.getTag(R.id.key_simple_recycler_view_adapter_position), v.getTag(R.id.key_sectioned_recycler_view_adapter_item)));
        return true;
    }

    public boolean isSubheader(int position) {
        for (int i = 0; i < sectionList.size(); i++) {
            if (sectionList.get(i).getSize() == 0) {
                continue;
            }

            int subheaderPosition = IntStream.range(0, i)
                    .map(new IntUnaryOperator() {
                        @Override
                        public int applyAsInt(int j) {
                            return sectionList.get(j).getSize();
                        }
                    })
                    .sum();

            if (subheaderPosition == position) {
                return true;
            }
        }

        return false;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        final ViewDataBinding binding;
    }

    public interface Section<H, I> {

        void setAdapter(SectionedRecyclerViewAdapter adapter);

        int getSize();

        void setSubheaderPositionSupplier(IntSupplier intSupplier);

        void addOnSubheaderChangedCallback();

        void addOnItemListChangedCallback();

        void removeOnSubheaderChangedCallback();

        void removeOnItemListChangedCallback();

        ObservableField<H> getSubheader();

        VariableLayoutPair getSubheaderVariableLayoutPair();

        ObservableList<I> getItemList();

        VariableLayoutPair getItemVariableLayoutPair();

        @Nullable
        Consumer<Pair<Integer, I>> getOnItemClick();

        @Nullable
        Consumer<Pair<Integer, I>> getOnItemLongClick();
    }
}