package com.movie.asahina.movie.view.linearLayout;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.annimon.stream.IntPair;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.movie.asahina.movie.R;
import com.movie.asahina.movie.view.Utils.VariableLayoutPair;

import java.util.Collection;



/**
 * データバインディング対応 RecyclerViewAdapter。セクション分け非対応。
 * <p>
 * 参考： https://github.com/radzio/android-data-binding-recyclerview
 */
public class SimpleLinearLayoutAdapter<T> {

	public SimpleLinearLayoutAdapter(@NonNull LinearLayout linearLayout, @NonNull Collection<T> itemCollection, @NonNull VariableLayoutPair variableLayoutPair, @Nullable Consumer<Pair<Integer, T>> onItemClick, @Nullable Consumer<Pair<Integer, T>> onItemLongClick) {
		this.context = linearLayout.getContext();

		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		this.linearLayout = linearLayout;

		this.variableLayoutPair = variableLayoutPair;

		this.onItemClick = onItemClick;

		this.onItemLongClick = onItemLongClick;

		if (itemCollection instanceof ObservableList) {
			itemList = (ObservableList<T>) itemCollection;
		} else {
			itemList = new ObservableArrayList<>();
			itemList.addAll(itemCollection);
		}

		itemList.addOnListChangedCallback(new ObservableList.OnListChangedCallback<ObservableList<T>>() {

			@Override
			public void onChanged(ObservableList<T> sender) {
				addView();
			}

			@Override
			public void onItemRangeChanged(ObservableList<T> sender, int positionStart, int itemCount) {
				addView();
			}

			@Override
			public void onItemRangeInserted(ObservableList<T> sender, int positionStart, int itemCount) {
				addView();
			}

			@Override
			public void onItemRangeMoved(ObservableList<T> sender, int fromPosition, int toPosition, int itemCount) {
				addView();
			}

			@Override
			public void onItemRangeRemoved(ObservableList<T> sender, int positionStart, int itemCount) {
				addView();
			}
		});
	}

	private final Context context;

	private final ObservableList<T> itemList;

	private final VariableLayoutPair variableLayoutPair;

	private final LinearLayout linearLayout;

	private LayoutInflater inflater;

	@Nullable
	private final Consumer<Pair<Integer, T>> onItemClick;

	@Nullable
	private final Consumer<Pair<Integer, T>> onItemLongClick;


	public View getView(int position) {

		T item = itemList.get(position);

		ViewDataBinding binding = DataBindingUtil.inflate(inflater, variableLayoutPair.layoutId, null, false);
		binding.setVariable(variableLayoutPair.variableId, item);

		binding.getRoot().setTag(R.id.key_simple_recycler_view_adapter_position, position);
		binding.getRoot().setTag(R.id.key_simple_recycler_view_adapter_item, item);

		if (onItemClick != null) {
			binding.getRoot().setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (onItemClick == null) {
						return;
					}

					onItemClick.accept(new Pair<>((Integer) v.getTag(R.id.key_simple_recycler_view_adapter_position), (T) v.getTag(R.id.key_simple_recycler_view_adapter_item)));
				}
			});
		}

		if (onItemLongClick != null) {
			binding.getRoot().setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					if (onItemLongClick == null) {
						return false;
					}

					onItemLongClick.accept(new Pair<>((Integer) v.getTag(R.id.key_simple_recycler_view_adapter_position), (T) v.getTag(R.id.key_simple_recycler_view_adapter_item)));
					return true;
				}
			});
		}

		return binding.getRoot();
	}

	public void addView() {

		linearLayout.removeAllViews();

		Stream.of(itemList)
				.indexed()
				.forEach(new Consumer<IntPair<T>>() {
					@Override
					public void accept(IntPair<T> pair) {
						linearLayout.addView(getView(pair.getFirst()));
					}
				});
	}
}