package com.movie.asahina.movie.view.overCoverView;

/**
 * Created by takuasahina on 2017/12/25.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.view.View;

import com.movie.asahina.movie.R;

import java.math.BigDecimal;

public class OverCoverView extends View {

	public OverCoverView(Context context) {
		super(context);
		init(context, null);
	}

	public OverCoverView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	public OverCoverView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs);
	}


	private int scaleWidth = 0;

	private int scaleHeight = 0;

	@ColorInt
	private int backgroundColor;

	private int viewWidth;

	private int viewHeight;

	private BigDecimal height;

	private BigDecimal width;

	private BigDecimal top = BigDecimal.ZERO;

	private BigDecimal bottom = BigDecimal.ZERO;

	private BigDecimal left = BigDecimal.ZERO;

	private BigDecimal right = BigDecimal.ZERO;


	private void init(Context context, AttributeSet attrs) {

		TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.overRideView, 0, 0);

		try {
			scaleWidth = typedArray.getInteger(R.styleable.overRideView_width_scale, 0);
			scaleHeight = typedArray.getInteger(R.styleable.overRideView_height_scale, 0);
		} finally {
			typedArray.recycle();
		}

		setFocusable(true);
		backgroundColor = getResources().getColor(R.color.blackOpacity87);
	}

	public void setScaleWidth(int scaleWidth) {
		this.scaleWidth = scaleWidth;
	}

	public void setScaleHeight(int scaleHeight) {
		this.scaleHeight = scaleHeight;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);

		if (scaleWidth == 0 && scaleHeight == 0) {
			width = new BigDecimal(getWidth());
			height = new BigDecimal(getHeight());
			return;
		}

		viewWidth = getWidth();
		viewHeight = getHeight();

		BigDecimal scale = new BigDecimal(viewWidth).divide(new BigDecimal(scaleWidth), 4, BigDecimal.ROUND_DOWN);
		BigDecimal height = scale.multiply(new BigDecimal(scaleHeight));

		if (new BigDecimal(viewHeight).subtract(height).compareTo(BigDecimal.ZERO) > 0) {

			this.left = BigDecimal.ZERO;
			this.top = (new BigDecimal(viewHeight).subtract(height)).divide(new BigDecimal(2), 4, BigDecimal.ROUND_DOWN);
			this.right = new BigDecimal(viewWidth);
			this.bottom = top.add(height);

			this.width = right;
			this.height = height;

		} else {

			scale = new BigDecimal(viewHeight).divide(new BigDecimal(scaleHeight), 4, BigDecimal.ROUND_DOWN);
			BigDecimal width = scale.multiply(new BigDecimal(scaleWidth));

			this.left = (new BigDecimal(viewWidth).subtract(width)).divide(new BigDecimal(2), 4, BigDecimal.ROUND_DOWN);
			this.top = BigDecimal.ZERO;
			this.right = left.add(width);
			this.bottom = new BigDecimal(viewHeight);

			this.width = width;
			this.height = bottom;
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {

		Paint paint = new Paint();
		paint.setColor(backgroundColor);

		Path path = new Path();
		path.addRect(0, 0, viewWidth, viewHeight, Path.Direction.CW);
		path.addRect(left.intValue(), top.intValue(), right.intValue(), bottom.intValue(), Path.Direction.CCW);
		canvas.drawPath(path, paint);
	}


	public int getViewX() {
		return left.intValue();
	}

	public int getViewY() {
		return top.intValue();
	}

	public int getViewWidth() {
		return width.intValue();
	}

	public int getViewHeight() {
		return height.intValue();
	}

	public Bitmap editBitmap(Bitmap bitmap) {
		return Bitmap.createBitmap(bitmap, getViewX(), getViewY(), getViewWidth(), getViewHeight(), null, true);
	}

	@BindingAdapter({"width_scale", "height_scale"})
	public static void setImageResource(OverCoverView view, int width_scale, int height_scale) {
		view.setScaleWidth(width_scale);
		view.setScaleHeight(height_scale);
		view.invalidate();
	}
}