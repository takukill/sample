package com.movie.asahina.movie.view.viewPager;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.movie.asahina.movie.R;

/**
 * Created by takuasahina on 2017/11/09.
 */

public class ViewPagerIndicator extends RadioGroup {

	public ViewPagerIndicator(Context context) {
		this(context, null);
	}

	public ViewPagerIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);

		setOrientation(LinearLayout.HORIZONTAL);
		setGravity(Gravity.CENTER);
	}


	private int size;

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		if (this.size == size) {
			return;
		}

		this.size = size;
		setCount(size);
	}


	private int position;

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		if (this.position == position) {
			return;
		}

		this.position = position;
	}

	/**
	 * ページ数をセットする
	 *
	 * @param count
	 */
	public void setCount(int count) {
		size = count;

		removeAllViews();

		Stream.range(0, count)
				.map(new Function<Integer, RadioButton>() {
					@Override
					public RadioButton apply(Integer integer) {
						RadioButton radioButton = new RadioButton(getContext());
						radioButton.setButtonDrawable(R.drawable.radio_indicater_drawable);
						radioButton.setClickable(false);
						radioButton.setFocusable(false);
						radioButton.setPadding(20, 0, 20, 20);
						addView(radioButton);
						return radioButton;
					}
				})
				.collect(Collectors.<RadioButton>toList());

		setCurrentPosition(position);
	}

	/**
	 * 現在の位置をセットする
	 *
	 * @param position
	 */
	public void setCurrentPosition(int position) {
		if (position >= size) {
			position = size - 1;
		}

		if (position < 0) {
			position = size > 0 ? 0 : -1;
		}

		if (position >= 0 && position < size) {
			// 現在の位置のRadioButtonをチェック状態にする
			RadioButton radioButton = (RadioButton) getChildAt(position);
			radioButton.setChecked(true);
			radioButton.invalidate();
		}
		invalidate();
	}

	@BindingAdapter("size")
	public static void setSize(ViewPagerIndicator view, int size) {
		view.setSize(size);
	}

	@BindingAdapter("position")
	public static void setPosition(ViewPagerIndicator view, int position) {
		view.setPosition(position);
	}
}