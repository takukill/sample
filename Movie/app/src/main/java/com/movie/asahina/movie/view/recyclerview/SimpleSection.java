package com.movie.asahina.movie.view.recyclerview;

import android.databinding.Observable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.databinding.ObservableList;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;

import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.IntSupplier;
import com.movie.asahina.movie.view.Utils.VariableLayoutPair;

import java.util.Collection;

/**
 * SectionedRecyclerViewAdapter に表示するシンプルなセクション。
 */
public class SimpleSection<H, I> implements SectionedRecyclerViewAdapter.Section<H, I> {

    //region ファクトリメソッド
    public static <H, I> SimpleSection<H, I> create(ObservableField<H> subheader, int subheaderVariableId, @LayoutRes int subheaderLayoutId, Collection<I> itemCollection, int itemVariableId, @LayoutRes int itemLayoutId, @Nullable Consumer<Pair<Integer, I>> onItemClick, @Nullable Consumer<Pair<Integer, I>> onItemLongClick) {
        return new SimpleSection<>(subheader, new VariableLayoutPair(subheaderVariableId, subheaderLayoutId), itemCollection, new VariableLayoutPair(itemVariableId, itemLayoutId), onItemClick, onItemLongClick);
    }

    public static <H, I> SimpleSection<H, I> create(ObservableField<H> subheader, int subheaderVariableId, @LayoutRes int subheaderLayoutId, Collection<I> itemCollection, int itemVariableId, @LayoutRes int itemLayoutId, @Nullable Consumer<Pair<Integer, I>> onItemClick) {
        return create(subheader, subheaderVariableId, subheaderLayoutId, itemCollection, itemVariableId, itemLayoutId, onItemClick, null);
    }

    public static <H, I> SimpleSection<H, I> create(ObservableField<H> subheader, int subheaderVariableId, @LayoutRes int subheaderLayoutId, Collection<I> itemCollection, int itemVariableId, @LayoutRes int itemLayoutId) {
        return create(subheader, subheaderVariableId, subheaderLayoutId, itemCollection, itemVariableId, itemLayoutId, null, null);
    }

    public static <H, I> SimpleSection<H, I> create(H subheader, int subheaderVariableId, @LayoutRes int subheaderLayoutId, Collection<I> itemCollection, int itemVariableId, @LayoutRes int itemLayoutId, @Nullable Consumer<Pair<Integer, I>> onItemClick, @Nullable Consumer<Pair<Integer, I>> onItemLongClick) {
        return create(new ObservableField<>(subheader), subheaderVariableId, subheaderLayoutId, itemCollection, itemVariableId, itemLayoutId, onItemClick, onItemLongClick);
    }

    public static <H, I> SimpleSection<H, I> create(H subheader, int subheaderVariableId, @LayoutRes int subheaderLayoutId, Collection<I> itemCollection, int itemVariableId, @LayoutRes int itemLayoutId, @Nullable Consumer<Pair<Integer, I>> onItemClick) {
        return create(new ObservableField<>(subheader), subheaderVariableId, subheaderLayoutId, itemCollection, itemVariableId, itemLayoutId, onItemClick, null);
    }

    public static <H, I> SimpleSection<H, I> create(H subheader, int subheaderVariableId, @LayoutRes int subheaderLayoutId, Collection<I> itemCollection, int itemVariableId, @LayoutRes int itemLayoutId) {
        return create(new ObservableField<>(subheader), subheaderVariableId, subheaderLayoutId, itemCollection, itemVariableId, itemLayoutId, null, null);
    }
    //endregion

    private SimpleSection(@NonNull ObservableField<H> subheader, @NonNull VariableLayoutPair subheaderVariableLayoutPair, @NonNull final Collection<I> itemCollection, @NonNull VariableLayoutPair itemVariableLayoutPair, @Nullable Consumer<Pair<Integer, I>> onItemClick, @Nullable Consumer<Pair<Integer, I>> onItemLongClick) {
        this.subheaderVariableLayoutPair = subheaderVariableLayoutPair;

        this.subheader = subheader;

        this.itemVariableLayoutPair = itemVariableLayoutPair;

        this.onItemClick = onItemClick;

        this.onItemLongClick = onItemLongClick;

        if (itemCollection instanceof ObservableList) {
            itemList = (ObservableList<I>) itemCollection;
        } else {
            itemList = new ObservableArrayList<>();
            itemList.addAll(itemCollection);
        }
    }

    private SectionedRecyclerViewAdapter adapter;

    private IntSupplier subheaderPositionSupplier;

    private Observable.OnPropertyChangedCallback onSubheaderChangedCallback;

    private final ObservableField<H> subheader;

    private final VariableLayoutPair subheaderVariableLayoutPair;

    private ObservableList.OnListChangedCallback<ObservableList<I>> onListChangedCallback;

    private final ObservableList<I> itemList;

    private final VariableLayoutPair itemVariableLayoutPair;

    @Nullable
    private final Consumer<Pair<Integer, I>> onItemClick;

    @Nullable
    private final Consumer<Pair<Integer, I>> onItemLongClick;

    @Override
    public void setAdapter(SectionedRecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    public int getSize() {
        return 0 < itemList.size()
                ? itemList.size() + 1
                : 0;
    }

    @Override
    public void setSubheaderPositionSupplier(IntSupplier intSupplier) {
        subheaderPositionSupplier = intSupplier;
    }

    @Override
    public void addOnSubheaderChangedCallback() {
        if (onSubheaderChangedCallback != null) {
            return;
        }

        onSubheaderChangedCallback = new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (itemList.isEmpty()) {
                    return;
                }

                adapter.notifyItemChanged(subheaderPositionSupplier.getAsInt());
            }
        };
        subheader.addOnPropertyChangedCallback(onSubheaderChangedCallback);
    }

    @Override
    public void addOnItemListChangedCallback() {
        if (onListChangedCallback != null) {
            return;
        }

        onListChangedCallback = new ObservableList.OnListChangedCallback<ObservableList<I>>() {

            private boolean isPreviousListEmpty = itemList.isEmpty();

            @Override
            public void onChanged(ObservableList<I> sender) {
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onItemRangeChanged(ObservableList<I> sender, int positionStart, int itemCount) {
                adapter.notifyItemRangeChanged((subheaderPositionSupplier.getAsInt() + 1) + positionStart, itemCount);
            }

            @Override
            public void onItemRangeInserted(ObservableList<I> sender, int positionStart, int itemCount) {
                if (isPreviousListEmpty) {
                    // 変更前のリストが空だった場合、subheader も追加する
                    adapter.notifyItemRangeInserted(subheaderPositionSupplier.getAsInt() + positionStart, itemCount + 1);
                } else {
                    adapter.notifyItemRangeInserted((subheaderPositionSupplier.getAsInt() + 1) + positionStart, itemCount);
                }

                isPreviousListEmpty = sender.isEmpty();
            }

            @Override
            public void onItemRangeMoved(ObservableList<I> sender, int fromPosition, int toPosition, int itemCount) {
                adapter.notifyItemMoved((subheaderPositionSupplier.getAsInt() + 1) + fromPosition, (subheaderPositionSupplier.getAsInt() + 1) + toPosition);
            }

            @Override
            public void onItemRangeRemoved(ObservableList<I> sender, int positionStart, int itemCount) {
                if (sender.isEmpty()) {
                    // 変更後のリストが空だった場合、subheader も削除する
                    adapter.notifyItemRangeRemoved(subheaderPositionSupplier.getAsInt() + positionStart, itemCount + 1);
                } else {
                    adapter.notifyItemRangeRemoved((subheaderPositionSupplier.getAsInt() + 1) + positionStart, itemCount);
                }

                isPreviousListEmpty = sender.isEmpty();
            }
        };
        itemList.addOnListChangedCallback(onListChangedCallback);
    }

    @Override
    public void removeOnSubheaderChangedCallback() {
        if (onSubheaderChangedCallback == null) {
            return;
        }

        subheader.removeOnPropertyChangedCallback(onSubheaderChangedCallback);
        onSubheaderChangedCallback = null;
    }

    @Override
    public void removeOnItemListChangedCallback() {
        if (onListChangedCallback == null) {
            return;
        }

        itemList.removeOnListChangedCallback(onListChangedCallback);
        onListChangedCallback = null;
    }

    @Override
    public ObservableField<H> getSubheader() {
        return subheader;
    }

    @Override
    public VariableLayoutPair getSubheaderVariableLayoutPair() {
        return subheaderVariableLayoutPair;
    }

    @Override
    public ObservableList<I> getItemList() {
        return itemList;
    }

    @Override
    public VariableLayoutPair getItemVariableLayoutPair() {
        return itemVariableLayoutPair;
    }

    @Nullable
    @Override
    public Consumer<Pair<Integer, I>> getOnItemClick() {
        return onItemClick;
    }

    @Nullable
    @Override
    public Consumer<Pair<Integer, I>> getOnItemLongClick() {
        return onItemLongClick;
    }
}
