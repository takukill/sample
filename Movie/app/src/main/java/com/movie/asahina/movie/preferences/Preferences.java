package com.movie.asahina.movie.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.$Gson$Types;
import com.movie.asahina.movie.BaseApplication;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * アプリの SharedPreferences
 */
public class Preferences {

	public Preferences(Context context) {
		this.context = context;
	}


	public static final String SHARED_PREFERENCES_NAME = "qiilo_preference";

	private final Context context;


	//region ユーザーID
	private static final String KEY_MY_USER_ID = "KEY_MY_USER_ID";

	public int getMyUserId() {
		return getInt(KEY_MY_USER_ID, -1);
	}

	public void setMyUserId(int userId) {
		setInt(KEY_MY_USER_ID, userId);
	}
	//endregion

	//region トークン
	private static final String KEY_MY_TOKEN = "KEY_MY_TOKEN";

	public String getToken() {
		return getString(KEY_MY_TOKEN,"");
	}

	public void setToken(String token) {
		setString(KEY_MY_TOKEN, token);
	}
	//endregion

	//region 本登録
	private static final String KEY_REGISTRATION = "KEY_REGISTRATION";

	public boolean getRegistration() {
		return getBoolean(KEY_REGISTRATION, false);
	}

	public void setRegistration(boolean registration) {
		setBoolean(KEY_REGISTRATION, registration);
	}
	//endregion

//	//region チャットメッセージ
//	private static final String KEY_CHAT_MESSAGE = "KEY_CHAT_MESSAGE";
//
//	public List<ChatItemViewModel> getChatMessageList(int negotiationId) {
//		List<ChatItemViewModel> negotiationList = getObjectList(KEY_CHAT_MESSAGE + negotiationId, ChatItemViewModel.class);
//		return negotiationList;
//	}
//
//	public void setChatMessageList(int negotiationId, List<ChatItemViewModel> messageDetails) {
//		setObject(KEY_CHAT_MESSAGE + negotiationId, messageDetails);
//	}
//	//endregion

	//region チャットステップの表示
	private static final String KEY_SHOW_CHAT_STEP = "KEY_SHOW_CHAT_STEP";

	public boolean getShowChatStep(int negotiationId) {
		return getBoolean(KEY_SHOW_CHAT_STEP + negotiationId, true);
	}

	public void setShowChatStep(int negotiationId, boolean show) {
		setBoolean(KEY_SHOW_CHAT_STEP + negotiationId, show);
	}
	//endregion


	//region ラッパーメソッドなどの定義
	private static SharedPreferences getPreference() {
		return BaseApplication.getContext().getSharedPreferences(SHARED_PREFERENCES_NAME, MODE_PRIVATE);
	}

	private static void setInt(String key, int value) {
		SharedPreferences.Editor editor = getPreference().edit();
		editor.putInt(key, value);
		editor.apply();
	}

	private static int getInt(String key, int defaultValue) {
		return getPreference().getInt(key, defaultValue);
	}

	private static void setBoolean(String key, boolean value) {
		SharedPreferences.Editor editor = getPreference().edit();
		editor.putBoolean(key, value);
		editor.apply();
	}

	private static boolean getBoolean(String key, boolean defaultValue) {
		return getPreference().getBoolean(key, defaultValue);
	}

	private static void setString(String key, String value) {
		SharedPreferences.Editor editor = getPreference().edit();
		editor.putString(key, value);
		editor.apply();
	}

	private static String getString(String key, String defaultValue) {
		return getPreference().getString(key, defaultValue);
	}

	private static void setObject(String key, Object value) {
		SharedPreferences.Editor editor = getPreference().edit();
		String json = createGson().toJson(value);
		editor.putString(key, json);
		editor.apply();
	}

	private static <T> T getObject(String key, Class<T> type) {
		String json = getPreference().getString(key, null);
		if (json == null) {
			return null;
		}

		return createGson().fromJson(json, type);
	}

	private static <T> List<T> getObjectList(String key, Class<T> type) {
		Type listType = $Gson$Types.newParameterizedTypeWithOwner(null, ArrayList.class, type);
		String json = getPreference().getString(key, null);
		if (json == null) {
			return new ArrayList<>();
		}

		return (List<T>) createGson().fromJson(json, listType);
	}

	private static Gson createGson() {
		GsonBuilder builder = new GsonBuilder();
		builder.enableComplexMapKeySerialization();
		return builder.create();
	}
	//endregion
}
