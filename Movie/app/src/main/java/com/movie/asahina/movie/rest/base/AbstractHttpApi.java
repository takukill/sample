package com.movie.asahina.movie.rest.base;

import android.support.annotation.NonNull;

import com.movie.asahina.movie.BaseApplication;
import com.movie.asahina.movie.preferences.Preferences;

import org.jdeferred.Deferred;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 通信API共通
 */

abstract public class AbstractHttpApi {

	private static final int CONNECTION_TIMEOUT = 1000;
	private static final int READ_TIMEOUT = 3000;
	private static final int WRITE_TIMEOUT = 3000;

	public static final Retrofit retrofit;

	public static final Retrofit gmoRetrofit;

	public static final Retrofit yahooRetrofit;

	public static final Retrofit resisterRetrofit;

	static {

		Preferences preferences = new Preferences(BaseApplication.getContext());

		OkHttpClient apiClient = new OkHttpClient().newBuilder()
				.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MINUTES)
				.readTimeout(READ_TIMEOUT, TimeUnit.MINUTES)
				.writeTimeout(WRITE_TIMEOUT, TimeUnit.MINUTES)
				.addInterceptor(
						new Interceptor() {
							@Override
							public okhttp3.Response intercept(Chain chain) throws IOException {
								Request original = chain.request();
								Request.Builder requestBuilder = original.newBuilder()
										.addHeader("Content-Type", "application/json")
										.addHeader("Accept", "application/json")

										// Authorization: YOUR_TOKEN_HERE→ユーザを一意に判定するためのトークン(Laravelで発行する十数桁のランダムな英数字をいれる想定)
										// TODO ユーザートークン
//										.addHeader("Authorization", "Bearer " + "aT56weHL09xb568R")
										// X-QIILO-OS: YOUR_OS_TYPE→iOS, Androidのどちらかを指定
										// TODO
//										.addHeader("X-QIILO-OS", "Android")
										.addHeader("X-QIILO-OS", "YOUR_OS_TYPE")

										// X-QIILO-OS-VERSION: YOUR_OS_VERSION→OSのバージョンを入れる
										// TODO
//										.addHeader("X-QIILO-OS-VERSION", BaseApplication.version)
										.addHeader("X-QIILO-OS-VERSION", "YOUR_OS_VERSION")

										// X-QIILO-APPLICATION-VERSION: YOUR_APPLICATION_VERSION→アプリケーションのバージョンを入れる
										// TODO
//										.addHeader("X-QIILO-APPLICATION-VERSION", BaseApplication.appVersion);
										.addHeader("X-QIILO-APPLICATION-VERSION", "YOUR_APPLICATION_VERSION");

								Request request = requestBuilder.build();
								return chain.proceed(request);
							}
						}
				)
				.build();

		retrofit = new Retrofit.Builder()
				.baseUrl(ApiConsts.BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.client(apiClient)
				.build();


		OkHttpClient gmoApiClient = new OkHttpClient().newBuilder()
				.addInterceptor(
						new Interceptor() {
							@Override
							public okhttp3.Response intercept(Chain chain) throws IOException {
								Request original = chain.request();
								Request.Builder requestBuilder = original.newBuilder();
								Request request = requestBuilder.build();
								return chain.proceed(request);
							}
						}
				)
				.build();

		gmoRetrofit = new Retrofit.Builder()
				.baseUrl(ApiConsts.GMO_BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.client(gmoApiClient)
				.build();


		OkHttpClient yahooApiClient = new OkHttpClient().newBuilder()
				.addInterceptor(
						new Interceptor() {
							@Override
							public okhttp3.Response intercept(Chain chain) throws IOException {
								Request original = chain.request();
								Request.Builder requestBuilder = original.newBuilder();
								Request request = requestBuilder.build();
								return chain.proceed(request);
							}
						}
				)
				.build();

		yahooRetrofit = new Retrofit.Builder()
				.baseUrl(ApiConsts.YAHOO_BASE)
				.addConverterFactory(GsonConverterFactory.create())
				.client(yahooApiClient)
				.build();


		OkHttpClient resisterApiClient = new OkHttpClient().newBuilder()
				.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MINUTES)
				.readTimeout(READ_TIMEOUT, TimeUnit.MINUTES)
				.writeTimeout(WRITE_TIMEOUT, TimeUnit.MINUTES)
				.addInterceptor(
						new Interceptor() {
							@Override
							public okhttp3.Response intercept(Chain chain) throws IOException {
								Request original = chain.request();
								Request.Builder requestBuilder = original.newBuilder()
										.addHeader("Content-Type", "application/json")
										.addHeader("Accept", "application/json");
								Request request = requestBuilder.build();
								return chain.proceed(request);
							}
						}
				)
				.build();

		resisterRetrofit = new Retrofit.Builder()
				.baseUrl(ApiConsts.BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.client(resisterApiClient)
				.build();
	}


	/**
	 * 通信結果のCallbackをPromiseで返す。
	 * MainThreadでハンドルしたいときは、APIの呼び元でnew AndroidDeferredManager().when(promise)します。
	 *
	 * @param call
	 * @return
	 */
	protected
	@NonNull
	Promise setCallback(@NonNull Call call) {
		final Deferred deferred = new DeferredObject();

		call.enqueue(new Callback<CommonResponse>() {
			@Override
			public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
				ResponseStatusEnum responseStatus = ResponseStatusEnum.getEnum(response.code());

				if (responseStatus == null) {
					deferred.reject(new HttpError(ResponseStatusEnum.SERVER_MAINTENANCE_ERROR, null));
					return;
				}

				switch (responseStatus) {
					case OK:
						// 正常
						deferred.resolve(response.body());
						break;
					default:
						// エラー
						deferred.reject(new HttpError(responseStatus, null));
						break;
				}
			}

			@Override
			public void onFailure(Call<CommonResponse> call, Throwable t) {
				// 接続、タイムアウトエラー
				deferred.reject(new HttpError(ResponseStatusEnum.SERVER_CONNECTION_ERROR, null));
			}
		});
		return deferred.promise();
	}
}
