//package com.movie.asahina.movie.model;
//
//import org.jdeferred.AlwaysCallback;
//import org.jdeferred.DoneCallback;
//import org.jdeferred.FailCallback;
//import org.jdeferred.Promise;
//
//import jp.co.carview.qiilo.model.base.BaseModel;
//import jp.co.carview.qiilo.rest.api.UserTemporaryApi;
//import jp.co.carview.qiilo.rest.base.HttpError;
//import jp.co.carview.qiilo.rest.dto.response.UserTemporary;
//
///**
// * Created by takuasahina on 2017/12/14.
// */
//
//public class UserInformationModel extends BaseModel {
//
//	public UserInformationModel(PreferencesModel preferencesModel, UserTemporaryApi userTemporaryApi) {
//
//		this.preferencesModel = preferencesModel;
//
//		this.userTemporaryApi = userTemporaryApi;
//
//		userId = preferencesModel.getMyUserId();
//	}
//
//
//	private final PreferencesModel preferencesModel;
//
//	private UserTemporaryApi userTemporaryApi;
//
//
//	//region userId
//	private int userId;
//
//	public int getUserId() {
//		return userId;
//	}
//
//	public void setUserId(int userId) {
//		this.userId = userId;
//		preferencesModel.setMyUserId(userId);
//	}
//	//endregion
//
//	//region temporaryUser
//	public static final String USER_TEMPORARY = "USER_TEMPORARY";
//
//	private UserTemporary userTemporary;
//
//	public UserTemporary getUserTemporary() {
//		return userTemporary;
//	}
//
//	public void setUserTemporary(UserTemporary userTemporary) {
//		this.userTemporary = userTemporary;
//		firePropertyChange(USER_TEMPORARY);
//	}
//	//endregion
//
//	public void temporaryUserResister() {
//		userTemporaryApi.request()
//				.done(new DoneCallback<UserTemporary>() {
//					@Override
//					public void onDone(UserTemporary result) {
//						setUserTemporary(result);
//					}
//				})
//				.fail(new FailCallback<HttpError>() {
//					@Override
//					public void onFail(HttpError result) {
//						setUserTemporary(null);
//					}
//				})
//				.always(new AlwaysCallback<UserTemporary, HttpError>() {
//					@Override
//					public void onAlways(Promise.State state, UserTemporary resolved, HttpError rejected) {
//
//					}
//				});
//	}
//}
//
//
