package com.movie.asahina.movie.rest.base;

import java.util.HashMap;
import java.util.Map;

/**
 * レスポンスステータスの列挙
 */
public enum ResponseStatusEnum {

	// 200 : 成功
	OK("0", 200),

	// 400 : リクエストの形式に問題がある(json parse error等)
	BODY_ERROR("1", 400),

	// 401 : 認証エラー
	CERTIFICATION_ERROR("2", 401),

	// 422 : リクエストの内容に問題がある
	REQUEST_ERROR("3", 422),

	// 426 : アプリケーションのアップグレードが必要
	UPGRADE_ERROR("4", 426),

	// 500 : サーバ側エラー
	SERVER_CONNECTION_ERROR("5", 500),

	// 503 : 一時的にサービス利用不可(メンテナンス等)
	SERVER_MAINTENANCE_ERROR("6", 503);


	ResponseStatusEnum(String code, int status) {
		this.code = code;
		this.status = status;
	}


	private static Map<Integer, ResponseStatusEnum> map;

	public final String code;

	public final int status;


	public static ResponseStatusEnum getEnum(int status) {
		if (map == null) {
			map = new HashMap<>();
			for (ResponseStatusEnum e : ResponseStatusEnum.values()) {
				map.put(e.status, e);
			}
		}

		return map.get(status);
	}
}
